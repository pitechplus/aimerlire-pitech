<?php
	$username = '';
	$password = '';
	$Eancode = @$_REQUEST['ean'];

	if($Eancode != '')
	{
		$urlSoap = 'https://mobile.payot.ch/AimerlireWS/AimerLireAccessHandler.svc?wsdl';

		$clientSOAP = new SoapClient($urlSoap);

		// print_r($clientSOAP->__getTypes());
		// print_r($clientSOAP->__getFunctions());

		$requestParams = array(
		    'Login' => $username,
		    'Password' => $password,
		    'Eancode' => $Eancode, // 9782070197873
		);

		try {
			$clientSOAP = new SoapClient($urlSoap);
			$soapResponse = $clientSOAP->GetProductDetail(array("request" => $requestParams));
			$returnArray = array();

			foreach ($soapResponse->GetProductDetailResult->Detail as $key => $value)
			{
				$returnArray[$key] = $value;
			}

			if(count($returnArray) > 0)
			{
				print_r($returnArray);
			}else
			{
				echo 'Wrong EAN code';
			}
		} catch (SoapFault $fault) {
			trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
		}
	}else
	{
		echo 'Need an EAN code';
	}
	
?>
