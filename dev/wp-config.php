<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */

//Disable Wordpress Cron
define('DISABLE_WP_CRON', true);

define( 'WP_DEBUG', false );

define('DB_NAME', 'aimerlire_development');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'aimerlire_wp');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'nufIT5FTnn');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/** Désactiver les mises à jour automatiques de Wordpress. */
define( 'WP_AUTO_UPDATE_CORE', false );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'frh<1T,=d0(8)r]Wqc=:jDDB9?40L|CYYY+jC24iqxY$}Yd8ipb;));/S963ny<!');
define('SECURE_AUTH_KEY',  '6:u7r%SPr$oRoBX[!dElye6!3/l,m~Infy&]ANpnO2@tS?;q>*AxvCHP!A/~U;]G');
define('LOGGED_IN_KEY',    'ciMNyw^f%+$N(IOFf(7X24O$ZsNAbO`{7 |7LBq/B=4lT]n5bLF:ItGAN6E2]iqo');
define('NONCE_KEY',        'o{B$]*,M3K#.9]@i9se8ulrIe.b/#Bpht>Z[29E*wplc e+[=%^8}-ir7{$zgm:s');
define('AUTH_SALT',        '_wj~fVwW9W<P;_QWWGCJ%m{R`71#w0Ha&jpK)*jYx|$uLY2nU(We I[NE!kt{awS');
define('SECURE_AUTH_SALT', 'XT0zU=}bPTT~[AQXSWvO=f$T5dfFb6s$B>[/ua$(=trf>S#$9!SqM^F#ngrP|>3v');
define('LOGGED_IN_SALT',   'gXcuu^@6?zBs&whI:sqLUlLZuMN+dO9}~uI:C#93|V&w#>5+HWE4<Xn}I(rH63XO');
define('NONCE_SALT',       '[_4VepDm$qN]xmQ|;3u?$zgiQtr?Lt~An3BYI@HKs1suZGO3%]M8eGL}EY9i2.KM');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d'information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
//define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');

//define('WP_HOME','http://dev.aimerlire.ch');
//define('WP_SITEURL','http://dev.aimerlire.ch');
