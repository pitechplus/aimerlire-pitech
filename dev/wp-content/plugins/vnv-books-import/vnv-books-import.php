<?php
/*
  Plugin Name: VNV Books Import
  Plugin URI: http://www.vnv.ch
  Description: Import books for aimelire.ch from an XLS file, a folder of picture and web service
  Version: 100.1.0
  Author: Sébastien Kapsopoulos and Fabien Zennaro
  Author URI: http://www.vnv.ch
  License: VNV249
  Text Domain: vnv-books-import
*/
 

define('DEFAULT_ATTACH_ID','39937');

 require(plugin_dir_path( __FILE__ ) ."lib/SimpleXLSX.php");
 require(plugin_dir_path( __FILE__ ) ."lib/VNVSoapPayot.php");

class Vnv_Books_Import{

  // Constructor
	function __construct() {
/*
		ini_set('display_errors', '1');
		error_reporting(E_ALL);
*/
		add_action( 'admin_menu', array( $this, 'wpa_add_menu' ));
		register_activation_hook( __FILE__, array( $this, 'wpa_install' ) );
		register_deactivation_hook( __FILE__, array( $this, 'wpa_uninstall' ) );
	}

	/*
	  * Actions perform at loading of admin menu
	  */
	function wpa_add_menu() {

		add_menu_page( 'Importation de livres', 'Import. de livres', 'manage_options', 'vnv-import-books-dashboard', array(
						  __CLASS__,
						 'wpa_dashboard'
						), 'dashicons-book-alt','2.2.9');

		/*add_submenu_page( 'vnv-import-books', 'Import Books simple' . ' Dashboard', ' Dashboard', 'manage_options', 'vnv-import-dashboard', array(
							  __CLASS__,
							 'wpa_page_file_path'
							));*/
	}

	/*
	 * Actions perform on loading of menu pages
	 */
	function wpa_dashboard() {
		include( plugin_dir_path( __FILE__ ) . "includes/dashboard.php");
	}

	/*
	 * Actions perform on activation of plugin
	 */
	function wpa_install() {



	}

	/*
	 * Actions perform on de-activation of plugin
	 */
	function wpa_uninstall() {



	}

}

new Vnv_Books_Import();


// $Vnv_Books_Import = new Vnv_Books_Import();
$VNVSoapPayot = new VNVSoapPayot();

function process_book($row, $ajax=false){
	global $VNVSoapPayot;

		$current = array();
		$errors = array();
		$messages = array();
		$ean = $row[0];
		if(trim($ean) == "")
		{
			$rows_skipped++;
			return null;
		}

		$ean = str_replace("\xc2\xa0",'',$ean);

		$books = get_posts(array("post_type" => array("book", "book_article"),
	 	'meta_key' => 'ean',
	 	'meta_value' => $ean,
		'numberposts' => -1,
		'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private'),
		));

		$postID = 0;
		if(count($books) > 0)
		{
			$postID = $books[0]->ID;
			$current['id']=$postID;
		}

		$wsData = array();
		$time_start = microtime_float();
		$wsData = $VNVSoapPayot->getBookDetails($ean);
		
		 
/*
		echo '<pre>';
		print_r($wsData);
		echo '</pre>';
		exit();
*/
		$wsFound = true;

		if(isset($wsData["error"]))
		{

			$wsData["Title"] = "";
			$wsData["Summary"] = "";
			$wsData["Subtitle"] = "";
			$wsData["Authors"] = new stdClass();
			$wsData["Authors"]->Author = array();
			$wsData["Editor"] = "";
			$wsData["SerieNr"] ="";
			$wsData["PagesCount"] = "";
			$wsData["Format"] = "";
			$wsData["PublicationDate"] = "";
			$wsData["Collection"] = "";
			$wsData["Availability"] = 0;
			$wsData["YoutubeVideoURL"] = "";
			$wsData["Serie"] = "";

			$wsFound = false;

			$errors['EAN']= "<strong>Unknown EAN. </strong>";
		}




		$book["title"] = $wsData["Title"];
		$book["content"] = $wsData["Summary"];
		$book["extra"]["sous-titre"] = $wsData["Subtitle"];
		$book["extra"]["type"] = $row[2];
		$book["extra"]["auteur"]  = "";
		$book["extra"]["auteurs_secondaires"]  = "";

		if(!is_array($wsData["Authors"]->Author))
		{
			$wsData["Authors"]->Author = array($wsData["Authors"]->Author);
		}

		foreach ($wsData["Authors"]->Author as $author) {
			if($author->IsPrincipal == 1)
			{
				if($book["extra"]["auteur"] != "")
				{
					$book["extra"]["auteur"] .=", ". $author->Firstname ." ". $author->Lastname;
				} else {
					$book["extra"]["auteur"] .= $author->Firstname ." ". $author->Lastname;
				}

			}
			else
			{
				if($book["extra"]["auteurs_secondaires"] != "")
				{
					$book["extra"]["auteurs_secondaires"] .= ", ";
				}

				$fonction = '';
				if($author->Fonction != NULL)
				{
					$fonction = ' ('.$author->Fonction.')';
				}

				$book["extra"]["auteurs_secondaires"] .= $author->Firstname ." ". $author->Lastname . $fonction;
			}
		}

		$book["extra"]["edition"] = $wsData["Editor"];
		$book["extra"]["texte_magazine"] = $row[7];
		$book["extra"]["categorie"] = $row[3];
		$book["extra"]["extrait"] =  $row[6];
		$book["extra"]["ean"] = $ean;
		$book["extra"]["lien_dachat"] = "http://www.payot.ch/Detail/".$book["extra"]["ean"];
		$book["extra"]["nom_serie"] = $wsData["Serie"];
		$book["extra"]["tome"] = $wsData["SerieNr"];
		$book["extra"]["nombre_de_pages"] = $wsData["PagesCount"];
		$book["extra"]["format"] = $wsData["Format"];
		$book["extra"]["date_de_parution"] = $wsData["PublicationDate"];
		$book["extra"]["collection"] = $wsData["Collection"];
		$book["extra"]["numero_magazine"] = $magazinNo;
		$book["extra"]["video"] = $wsData["YoutubeVideoURL"];
		$book["extra"]["disponibilite"] = $wsData["Availability"];


		if($book["content"] == null)
		{
			$book["content"] = $book["extra"]["texte_magazine"];
		}

		if($book["title"] == null)
		{
			$book["title"] = $row[4];
		}

		$book["image"] = get_wp_installation()."/wp-content/uploads/import/images/".$row[0].".jpg";
		if(!file_exists($book["image"]))
		{
			//$book["image"] = get_wp_installation()."/wp-content/uploads/import/images/couvertureNonDisponible.jpg";
			$errors['IMAGE']= "<strong>Image not found.</strong>";
		}


		$id=createPostFromArray($book, $row[1], $postID, $wsFound);

		$time_end = microtime_float();
		

		$time = round(($time_end - $time_start),3);
		if($ajax){
			$messages[] = '<tr>';
			if($id){
				//we had an existing item in our database, which means we updated it	
				$messages[] = '<td><span class="added-book">Added</span></td>';
			}elseif($current){
				$messages[] = '<td><span class="updated-book">Updated</span></td>';
			}
			$messages[] = '<td class="ean">'.$ean.'</td> ';
			$messages[] = '<td  class="book-title">'.$book["title"].'</td>, ';
			$messages[] = '<td class="book-author">'.$book["extra"]["auteur"].'</td>';
			$messages[] = '<td class="errors">';
			foreach ($errors as $err){
				$messages[] = $err;
			}	
			$messages[] = '</td>';	
			$messages[] = '<td class="time">'.$time.'s</td> ';
			$messages[] = '</tr>';
		}else{
			$messages[] = '<div class="book_process_result updated">';
			$messages[] = '<div class="book_info">';
			if($id){
				//we had an existing item in our database, which means we prolly updated it	
				$messages[] = '<span class="info"><span class="added-book">Added</span>: </span>';
			}elseif($current){
				$messages[] = '<span class="info"><span class="updated-book">Updated</span>: </span>';
			}
			$messages[] =  '<span class="ean">[EAN: '.$ean.' ]</span> ';
			$messages[] =  '<span class="title">'.$book["title"].'</span>, ';
			$messages[] =  '<span class="author">'.$book["extra"]["auteur"].'</span>';
			$messages[] = '</div>';			
			if($errors){
				$messages[] = '<div class="errors">';
				foreach ($errors as $err){
					$messages[] = $err;
				}
				$messages[] = '</div>';
			}		
			$messages[] = '</div>';
		}
			$messages = implode('',$messages);	
			if($ajax) {
				header('Content-type: application/json');
				echo json_encode(array($ean, $messages));
				wp_die();
			}
			elseif(!wp_doing_cron())
				echo $messages;	
	
}

 function handleXlSFile($file, $magazinNo,$ajax=false)
{
	
	

	$xlsx = new SimpleXLSX($file);
	$rows = $xlsx->rows();
	$rows_skipped=0;
	$current ;	$errors ; $messages ;
	if(empty($rows)){
		return;
	}
	
	if ($ajax) { 
		/* it's an AJAX call */ 
		$rows_json = json_encode($rows);
		header('Content-type: application/json');
		echo $rows_json;
		wp_die();
	}else{
		?>
	
	<?php
		foreach($rows as $row){
			
			process_book($row);
		}
	}

}

add_action('admin_head', 'aimer_custom_style');

function aimer_custom_style() {
  echo '<style>

 

  .added-book{color:#46b450; font-weight:bold;}
  .updated-book{color:orange; font-weight:bold;}

  .errors{
	  color:red;
  }

.book-import-info{
	font-size:1.3em;
	margin:20px 0;
}

.book-author{
	font-weight:normal;
	font-style:italic;
}
.book-title{
	font-weight:bold;
}
#cancel_import{visibility:hidden;}
#import-parameters{
	padding:10px; 
	display:inline-block; 
	margin:10px; 
	border: 1px solid #eee; 
	background:#fff; 
	vertical-align:top
}
#link-import-parameters{display:inline-block; margin:10px 0; vertical-align:top}
#import-parameters label {padding:5px;}
.has-tooltip{
position:relative;
}
.has-tooltip .tooltip{
	display:none; 
}
.has-tooltip:hover .tooltip{
	display:inline-block; 
	position:absolute;
	 width:120px;
	 padding:3px;
	top:100%;
	background:#555;
	color:#fff;
	font-size:0.9em;
	right:0;	
}

.time-remaining{
	margin:10px 0;
	padding:10px;
	display:inline-block; 
	border: 1px solid #eee; 
	background:#fff; 
	vertical-align:top;
}
  </style>';
}

add_action( 'admin_footer', 'my_action_javascript' ); // Write our JS below here

function my_action_javascript() { ?>
	<script type="text/javascript" >
	var booksToImport = [];

	var booksQueue = [];
	var maxQueue = 3;
	var throttle=0;
	var startTime;
	var booksCount = 0;
	
	jQuery(document).ready(function($) {

		$('#submit_import').on('click',function(ev){
			stopProcess = false;
			$('#results-table').hide();
			clearBooksTable();
			clearMessages();
			resetInterface();
			booksQueue = [];

			maxQueue = parseInt($('[name="queue_depth"]').val());
			throttle = parseInt($('[name="throttle"]').val())*1000;
			 

			$('.import-parameters-wrapper').hide();
			$('#import-parameters').css('visibility','hidden');	
			ev.preventDefault();
			$('#cancel_import').css('visibility','visible');
			displayMessage('<div class="info">Reading xlsx file...</div>', false);
			var data = {
			'action': 'process_xls',
			'magazine':$('[name="magazine"]').val()
			}
			$.post(ajaxurl,data,function(response){
				
				booksToImport = response;
				
				importBooks();
				
			});
			
		});

		$('#cancel_import').on('click',function(ev){
			ev.preventDefault();
			
			stopProcess = true;
			stopActions();			
			displayMessage('<div class="error">Stopped execution. Clearing queue...</div>',true);
			resetInterface();
			
		});
		$('#link-import-parameters').on('click',function(e){
			e.preventDefault();
			$('#import-parameters').css('visibility','visible');			
		});
	});

	function resetInterface(){
		$('.import-parameters-wrapper').show();
			$('.time-remaining').hide();
			$('.time-remaining .time').html('computing...');
			clearInterval(countdownTimer);
			remainingSeconds = 0;
			validBooksCount = booksCount = 0;
			$('#cancel_import').css('visibility','hidden');
	}

var validBooksCount;
	async function importBooks(){
						
		
		validBooksCount = booksCount = booksToImport.length;
		
		displayMessage('<div class="info">File contains <strong>'+booksCount+
		'</strong> rows.</div><div class="book-import-info">Starting import, this will take a while.</div>', false);
		if(throttle)
		displayMessage('<div class="info">To prevent server overload, execution will be paused for <strong>'+(throttle/1000)+
		'</strong> seconds after each book.</div>', false);
		$('#results-table').show();
		$('.time-remaining').show();
		startTime =  new Date() / 1000;
		secondsRemaining=0;
		countdownTimer = setInterval('timer()', 1000);
		
		var action = null;		
		for (var i = 0; i<booksCount; i++){
			

			var data = {
			'action': 'import_book',
			'book':booksToImport[i]
			}
			if (booksToImport[i][0]=='EAN') {
				validBooksCount --;
				continue; //skip header row.
			}

			if(await waitQueue([booksToImport[i][0],null])){
				
			action =	$.post(ajaxurl,data,function(response){
					removeQueue(response[0]);
					displayBookImportResult(response[1]);
					calculateTimeRemaining(i-(booksCount-validBooksCount));
				});
				assignAction(booksToImport[i][0],action);
			}
			else{
				break;
			}
		}
		resetInterface();
	}

	function sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}
	function stopActions(){
		var len = booksQueue.length
		for(var i = 0; i<len;i++){
			if(booksQueue[i][1])
			booksQueue[i][1].abort();
		}
	}

	function calculateTimeRemaining(currentBook){
	
		var booksRemaining = validBooksCount - currentBook;
		
		var elapsed = (new Date()/1000) - (startTime-throttle/1000);
		if (currentBook>1){
			secondsRemaining = (elapsed/currentBook) * booksRemaining;	
		}
	}

	var stopProcess=false;
	function removeQueue(item){
		if(booksQueue instanceof Array ){
			booksQueue = booksQueue.filter(function(x) {
				return x[0] != item;
			});			
		}
	}
	function assignAction(ean, action){
		if(booksQueue instanceof Array ){
			booksQueue  = booksQueue.map(function(item) { return item[0] == ean ? [ean, action] : item; });
		}
	}


	async function waitQueue(item){
		
		var isBusy = true;
		while(isBusy){
			if (stopProcess) {				
				return false;
			}
			if(booksQueue.length>=maxQueue) {
				await sleep(100);
			}else {
				if(throttle!=0)
				await  sleep(throttle);
				booksQueue.push(item);
				isBusy=false;
				return true;
			}
		}
	}
	function displayBookImportResult(book){
		jQuery('#results-body').append(book);
	}
	function clearBooksTable(){
		jQuery('#results-body').html('');
	}
	
	function displayMessage(message, prepend){
	if(prepend)	jQuery('#import_result').prepend(message);
	else jQuery('#import_result').append(message);
	}
	function clearMessages(){
		jQuery('#import_result').html('');
	}


	
var secondsRemaining = 0;
function timer() {
	if(secondsRemaining>0){
		var hours       = Math.floor(secondsRemaining/3600);
		var minutesLeft = Math.floor(secondsRemaining - (hours*3600));
		var minutes     = Math.floor(minutesLeft/60);
		
		if (minutes < 10) {
			minutes = "0" + minutes; 
		}
		if (hours < 10) {
			hours = "0" + hours; 
		}
		var remainingSeconds = Math.floor(secondsRemaining % 60);
		if (remainingSeconds < 10) {
			remainingSeconds = "0" + remainingSeconds; 
		}

				$('.time-remaining .time').html(hours+':'+minutes+':'+remainingSeconds);
	//  document.getElementById('countdown').innerHTML =  hours + ":" + minutes + ":" + remainingSeconds;
		secondsRemaining--;
		if (secondsRemaining == 0) {
			clearInterval(countdownTimer);
		//  document.getElementById('countdown').innerHTML = "Completed";
		}
	}
}
var countdownTimer;

	</script> <?php
}
// Same handler function...
add_action( 'wp_ajax_process_xls', 'ajax_process_xls' );
function ajax_process_xls() {
	
	if(!is_admin())
		wp_die();
	$magazine = intval( $_POST['magazine'] );

	if(is_int($magazine) && $magazine>0 && $magazine<8){
		handleXlSFile(WP_CONTENT_DIR."/uploads/import/Aimerlire_Magazine{$magazine}.xlsx",$magazine,true);
	}	

	wp_die();
}
function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

// Same handler function...
add_action( 'wp_ajax_import_book', 'ajax_import_book' );
function ajax_import_book() {
	
	if(!is_admin())
		wp_die();
	$row = $_POST['book'];

	if(is_array($row)){
		echo process_book($row, $ajax=true);
	}
	else{
		echo 'error parsing input data';
	}

	wp_die();
}



function createPostFromArray($data,$new,$postID,$wsFound) // $postID != 0 = update
{

	//echo "created";
	if($new == 1)
	{
		$post_type = "book";
	}
	else
	{
		$post_type = "book_article";
	}

	$post_status = "publish";

	if(!$wsFound)
	{
		$post_status = "draft";
	}

	if($postID > 0)
	{
		wp_update_post(array('ID' => $postID,
		"post_type" => $post_type,
		 "post_title" => $data["title"],
		 "post_content" => $data["content"],
		 "post_status" => $post_status ,
		 ));
	}
	else
	{
			$postID = wp_insert_post(array("post_type" => $post_type,
		 "post_title" => $data["title"],
		 "post_content" => $data["content"],
		 "post_status" => $post_status ,
		 ));
	}



	foreach($data["extra"] as $k => $v)
	{
		__update_post_meta($postID,$k,$v);
	}
	if($data["image"] != "")
	{
		Generate_Featured_Image($data["image"],$postID);
	}

}

if (!function_exists('__update_post_meta')){
function __update_post_meta( $post_id, $field_name, $value = '' )
{
	if ( empty( $value ) OR ! $value )
	{
		delete_post_meta( $post_id, $field_name );
	}
	elseif ( ! get_post_meta( $post_id, $field_name ) )
	{
		add_post_meta( $post_id, $field_name, $value );
	}
	else
	{
		update_post_meta( $post_id, $field_name, $value );
	}
}
}

function Generate_Featured_Image( $image_path, $post_id  ){
	$attach_id = DEFAULT_ATTACH_ID;
	if(file_exists($image_path)){
		$upload_dir = wp_upload_dir();
		$image_data = file_get_contents($image_path);
		$filename = basename($image_path);
		if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
		else                                    $file = $upload_dir['basedir'] . '/' . $filename;
		file_put_contents($file, $image_data);

		$wp_filetype = wp_check_filetype($filename, null );
		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_title' => sanitize_file_name($filename),
			'post_content' => '',
			'post_status' => 'inherit'
		);
		$attach_id = wp_insert_attachment( $attachment, $file, $post_id );
		require_once(ABSPATH . 'wp-admin/includes/image.php');
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
		$res1= wp_update_attachment_metadata( $attach_id, $attach_data );
	}
    $res2= set_post_thumbnail( $post_id, $attach_id );
}


function runMagCronJob1 () {
    handleXlSFile(get_wp_installation()."/wp-content/plugins/vnv-books-import/import/Aimerlire_Magazine1.xlsx",1);
} add_action('runMagCronJob1', 'runMagCronJob1');


function runMagCronJob2 () {
    handleXlSFile(get_wp_installation()."/wp-content/plugins/vnv-books-import/import/Aimerlire_Magazine2.xlsx",2);
} add_action('runMagCronJob2', 'runMagCronJob2');

function runMagCronJob3 () {
    handleXlSFile(get_wp_installation()."/wp-content/plugins/vnv-books-import/import/Aimerlire_Magazine3.xlsx",3);
} add_action('runMagCronJob3', 'runMagCronJob3');

function runMagCronJob4 () {
    handleXlSFile(get_wp_installation()."/wp-content/plugins/vnv-books-import/import/Aimerlire_Magazine4.xlsx",4);
} add_action('runMagCronJob4', 'runMagCronJob4');

function runMagCronJob5 () {
    handleXlSFile(get_wp_installation()."/wp-content/plugins/vnv-books-import/import/Aimerlire_Magazine5.xlsx",5);
} add_action('runMagCronJob5', 'runMagCronJob5');

function runMagCronJob6 () {
    handleXlSFile(get_wp_installation()."/wp-content/plugins/vnv-books-import/import/Aimerlire_Magazine6.xlsx",6);
} add_action('runMagCronJob6', 'runMagCronJob6');
function runMagCronJob7 () {
    handleXlSFile(get_wp_installation()."/wp-content/plugins/vnv-books-import/import/Aimerlire_Magazine7.xlsx",7);
} add_action('runMagCronJob7', 'runMagCronJob7');
function runMagCronJob8 () {
    handleXlSFile(get_wp_installation()."/wp-content/plugins/vnv-books-import/import/Aimerlire_Magazine8.xlsx",8);
} add_action('runMagCronJob8', 'runMagCronJob8');



