<?php
set_time_limit ( 9900 );

$Vnv_Books_Import = new Vnv_Books_Import();
$VNVSoapPayot = new VNVSoapPayot();

?>

<div class="wrap">
<h1>Importation de livres</h1>
<?php

	/*$books = get_posts(array("post_type" => array("book", "book_article"),
	 	'meta_key' => 'ean',
	 	'meta_value' => '9782070417940',
		'numberposts' => -1,
		'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private'),
		'orderby' => 'post_status',
		'order' => 'ASC'
		));

	/*$books = get_posts(array("post_type" => array("book", "book_article"),
		'numberposts' => -1,
		'post_status' =>  array('pulish')
		));

	foreach($books as $book)
	{
		if($book->post_type == 'book_article')
		{
			echo $book->post_type.'<br>';
			echo $book->post_status.'<br>';
		//	wp_delete_post($book->ID);
		}

	}
	/**/

if(isset($_POST["magazine"]))
{

	if($_POST["magazine"] == 1)
	{
		handleXlSFile(WP_CONTENT_DIR."/uploads/import/Aimerlire_Magazine1.xlsx",1,$VNVSoapPayot);
	}
	elseif($_POST["magazine"] == 2)
	{
		handleXlSFile(WP_CONTENT_DIR."/uploads/import/Aimerlire_Magazine2.xlsx",2,$VNVSoapPayot);
	}
	elseif($_POST["magazine"] == 3)
	{
		handleXlSFile(WP_CONTENT_DIR."/uploads/import/Aimerlire_Magazine3.xlsx",3,$VNVSoapPayot);
	}
	elseif($_POST["magazine"] == 4)
	{
		handleXlSFile(WP_CONTENT_DIR."/uploads/import/Aimerlire_Magazine4.xlsx",4,$VNVSoapPayot);
	}
	elseif($_POST["magazine"] == 5)
	{
		handleXlSFile(WP_CONTENT_DIR."/uploads/import/Aimerlire_Magazine5.xlsx",5,$VNVSoapPayot);
	}
	elseif($_POST["magazine"] == 6)
	{
		handleXlSFile(WP_CONTENT_DIR."/uploads/import/Aimerlire_Magazine6.xlsx",6,$VNVSoapPayot);
	}
	elseif($_POST["magazine"] == 7)
	{
		handleXlSFile(WP_CONTENT_DIR."/uploads/import/Aimerlire_Magazine7.xlsx",7,$VNVSoapPayot);
	}
	else
	{
		die("unknown magazin: ".$_POST["magazine"]);
	}

	if(isset($_FILES['xls_file']) ){
		$xls = $_FILES['xls_file'];

		$uploaded=media_handle_upload('xls_file', 0);
		// Error checking using WP functions
		if(is_wp_error($uploaded)){
				echo "Error uploading file: " . $uploaded->get_error_message();
		}else{
			$file = $xls["tmp_name"];
			$xlsx = new SimpleXLSX($file);
			print_r( $xlsx->rows() );
		}
	}
}
else
{
	?>
		<form action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="post" enctype="multipart/form-data">
			<!--<input type='file' id='xls_file' name='xls_file' required=""></input><br>-->
			<label for="magazine">N° Magazine</label>
			<select name="magazine" required="">
				<?php 
				for($i = 1; $i<8;$i++){
					echo '<option value="'.$i .'">Magazine #'.$i.'</option>';
				}
				?>
			</select>
			<input type="button" id="submit_import" value ="Import"> 
			<input type="button" id="cancel_import" value ="Stop">
			<div class="import-parameters-wrapper">
				<a href="#" id="link-import-parameters" >Import parameters</a>
				<div id="import-parameters" style="visibility:hidden">
					<label for="queue_depth">Process 
						<select name="queue_depth" required="">
							<?php 
							for($i = 1; $i<20;$i++){
								echo '<option value="'.$i .'" '.($i==1?'selected':'').'>'.$i.' books </option>';
							}
							?>
						</select> 
						simultaneously
					</label><br>
						
					<label for="throttle">Delay execution of each book for 
					<select name="throttle" required="">
					<?php 
					for($i = 0; $i<20;$i++){
						echo '<option value="'.$i .'" '.($i==3?'selected':'').'>'.$i.' seconds</option>';
					}
					?>
					</select>
				</label>
				</div>
			</div>
		</form>
		<div>
			<div class="time-remaining" style="display:none;">Estimated time remaining: <span class="time">calculating...</span></div>
		</div>
		<div id="import_result"></div>
		<table id="results-table" style="display:none" class="wp-list-table widefat fixed striped">
		<thead>
		<tr>
				<th class="column-primary" style="width:70px"><?= __('Action','aimer')?></th>
				<th class="column-primary" style="width:120px"><?= __('EAN','aimer')?></th>
				<th class="column-primary" ><?= __('Title','aimer')?></th>
				<th class="column-primary" ><?= __('Author','aimer')?></th>
				<th class="column-primary" ><?= __('Errors','aimer')?></th>
				<th class="column-primary has-tooltip" style="width:80px"><?= __('Time<i class="dashicons dashicons-editor-help"></i><span class="tooltip">Total time spent by the server processing this book. Does not include pause between books, if set.</span>','aimer')?></th>
			</tr>
			</thead>
			<tbody id="results-body">
  
 </tbody>
		</table>
	<?php
}

?>
</div>
<?php

?>
