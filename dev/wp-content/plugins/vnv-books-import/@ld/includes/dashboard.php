<?php
set_time_limit ( 300 );

$Vnv_Books_Import = new Vnv_Books_Import();
$VNVSoapPayot = new VNVSoapPayot();

?>

<div class="wrap">
<h1>Importation de livres</h1>
<?php

	/*$books = get_posts(array("post_type" => array("book", "book_article"),
	 	'meta_key' => 'ean',
	 	'meta_value' => '9782070417940',
		'numberposts' => -1,
		'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private'),
		'orderby' => 'post_status',
		'order' => 'ASC'
		));

	/*$books = get_posts(array("post_type" => array("book", "book_article"),
		'numberposts' => -1,
		'post_status' =>  array('pulish')
		));

	foreach($books as $book)
	{
		if($book->post_type == 'book_article')
		{
			echo $book->post_type.'<br>';
			echo $book->post_status.'<br>';
		//	wp_delete_post($book->ID);
		}
		
	}
	/**/



if(isset($_POST["magazine"]))
{
	


	if($_POST["magazine"] == 1)
	{
		handleXlSFile(plugin_dir_path( __FILE__ ) ."../import/Aimerlire_Magazine1.xlsx",1,$VNVSoapPayot);

	}
	elseif($_POST["magazine"] == 2)
	{
		handleXlSFile(plugin_dir_path( __FILE__ ) ."../import/Aimerlire_Magazine2.xlsx",2,$VNVSoapPayot);
	}	
	elseif($_POST["magazine"] == 3)
	{
		handleXlSFile(plugin_dir_path( __FILE__ ) ."../import/Aimerlire_Magazine3.xlsx",3,$VNVSoapPayot);
	}	
	elseif($_POST["magazine"] == 4)
	{
		handleXlSFile(plugin_dir_path( __FILE__ ) ."../import/Aimerlire_Magazine4.xlsx",4,$VNVSoapPayot);
	}	
	elseif($_POST["magazine"] == 5)
	{
		handleXlSFile(plugin_dir_path( __FILE__ ) ."../import/Aimerlire_Magazine5.xlsx",5,$VNVSoapPayot);
	}	
	elseif($_POST["magazine"] == 6)
	{
		handleXlSFile(plugin_dir_path( __FILE__ ) ."../import/Aimerlire_Magazine6.xlsx",6,$VNVSoapPayot);
	}	
	elseif($_POST["magazine"] == 7)
	{
		handleXlSFile(plugin_dir_path( __FILE__ ) ."../import/Aimerlire_Magazine7.xlsx",7,$VNVSoapPayot);
	}
	else
	{
		die("unknown magazin: ".$_POST["magazine"]);
	}
	
	if(isset($_FILES['xls_file']) ){
		$xls = $_FILES['xls_file'];
 
		$uploaded=media_handle_upload('xls_file', 0);
		// Error checking using WP functions
		if(is_wp_error($uploaded)){
				echo "Error uploading file: " . $uploaded->get_error_message();
		}else{
			
			$file = $xls["tmp_name"];
			$xlsx = new SimpleXLSX($file);
			print_r( $xlsx->rows() );		
		} 
	}
}
else
{
	?>
		<form action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>" method="post" enctype="multipart/form-data">
			<!--<input type='file' id='xls_file' name='xls_file' required=""></input><br>-->
			<label for="magazine">N° Magazine</label>
			<input type="text" name="magazine" required="">
			<input type="submit">
		</form>
	<?php
}

?>
</div>
<?php
function handleXlSFile($file, $magazinNo, $VNVSoapPayot)
{
	// Delete all books for this magazin
	//wp_delete_post();
	/*
	$books = get_posts(array("post_type" => array("book", "book_article"),
	 	'meta_key' => 'numero_magazine',
	 	'meta_value' => 3,
		'numberposts' => -1,
		'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private') 
		));

	foreach ($books as $book) {
		wp_delete_post($book->ID);
	}
	// Books deleted
	die();
	/**/





	$xlsx = new SimpleXLSX($file);
	$rows = $xlsx->rows();
	for($i = 1; $i < count($rows); $i++)
	{

		//if(trim($rows[$i][0]) == "" || $rows[$i][0] !=  '9782070113682 ')
		if(trim($rows[$i][0]) == "")
		{
			continue;
		}

		$rows[$i][0] = str_replace("\xc2\xa0",'',$rows[$i][0]);

		$books = get_posts(array("post_type" => array("book", "book_article"),
	 	'meta_key' => 'ean',
	 	'meta_value' => $rows[$i][0],
		'numberposts' => -1,
		'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private'),
		));

		$postID = 0;
		if(count($books) > 0)
		{
			$postID = $books[0]->ID;
		}

		$wsData = array();


		$wsData = $VNVSoapPayot->getBookDetails($rows[$i][0]);

		$wsFound = true;

		if(isset($wsData["error"]))
		{
			
			$wsData["Title"] = "";
			$wsData["Summary"] = "";
			$wsData["Subtitle"] = "";
			$wsData["Authors"] = new stdClass();
			$wsData["Authors"]->Author = array();
			$wsData["Editor"] = "";
			$wsData["SerieNr"] ="";
			$wsData["PagesCount"] = "";
			$wsData["Format"] = "";
			$wsData["PublicationDate"] = "";
			$wsData["Collection"] = "";
			$wsData["Availability"] = 0;
			$wsData["YoutubeVideoURL"] = "";
			$wsData["Serie"] = "";

			$wsFound = false;

			echo "<strong>Book: with ean ".$rows[$i][0]." WS: unknown EAN.</strong><br>";
		}




		$book["title"] = $wsData["Title"];
		$book["content"] = $wsData["Summary"]; 
		$book["extra"]["sous-titre"] = $wsData["Subtitle"];
		$book["extra"]["type"] = $rows[$i][2];
		$book["extra"]["auteur"]  = "";
		$book["extra"]["auteurs_secondaires"]  = "";

		if(!is_array($wsData["Authors"]->Author))
		{
			$wsData["Authors"]->Author = array($wsData["Authors"]->Author);
		}

		foreach ($wsData["Authors"]->Author as $author) {
			if($author->IsPrincipal == 1)
			{
				if($book["extra"]["auteur"] != "")
				{
					$book["extra"]["auteur"] .=", ". $author->Firstname ." ". $author->Lastname;
				} else {
					$book["extra"]["auteur"] .= $author->Firstname ." ". $author->Lastname;
				}
				
			}
			else
			{
				if($book["extra"]["auteurs_secondaires"] != "")
				{
					$book["extra"]["auteurs_secondaires"] .= ", ";
				}

				$fonction = '';
				if($author->Fonction != NULL)
				{
					$fonction = ' ('.$author->Fonction.')';
				}

				$book["extra"]["auteurs_secondaires"] .= $author->Firstname ." ". $author->Lastname . $fonction;
			}			
		}

		$book["extra"]["edition"] = $wsData["Editor"];
		$book["extra"]["texte_magazine"] = $rows[$i][7];
		$book["extra"]["categorie"] = $rows[$i][3];
		$book["extra"]["extrait"] =  $rows[$i][6];
		$book["extra"]["ean"] = $rows[$i][0];
		$book["extra"]["lien_dachat"] = "http://www.payot.ch/Detail/".$book["extra"]["ean"];
		$book["extra"]["nom_serie"] = $wsData["Serie"];
		$book["extra"]["tome"] = $wsData["SerieNr"];
		$book["extra"]["nombre_de_pages"] = $wsData["PagesCount"];
		$book["extra"]["format"] = $wsData["Format"];
		$book["extra"]["date_de_parution"] = $wsData["PublicationDate"];
		$book["extra"]["collection"] = $wsData["Collection"];
		$book["extra"]["numero_magazine"] = $magazinNo;
		$book["extra"]["video"] = $wsData["YoutubeVideoURL"];
		$book["extra"]["disponibilite"] = $wsData["Availability"];


		if($book["content"] == null)
		{
			$book["content"] = $book["extra"]["texte_magazine"];
		}

		if($book["title"] == null)
		{
			$book["title"] = $rows[$i][4];	
		}

		$book["image"] = plugin_dir_path( __FILE__ ) ."../import/images/".$rows[$i][0].".jpg";
		if(!file_exists($book["image"]))
		{
			$book["image"] = plugin_dir_path( __FILE__ ) ."../import/images/couvertureNonDisponible.jpg";
			echo "<strong>Book: with ean ".$rows[$i][0]." Image not found.</strong><br>";
		}


		createPostFromArray($book, $rows[$i][1], $postID, $wsFound);
		//echo "Book: with ean ".$rows[$i][0]." successfully created.<br>";


		if($i > 3 )
		{
			
		}
	}
}


function createPostFromArray($data,$new,$postID,$wsFound) // $postID != 0 = update
{


	if($new == 1)
	{
		$post_type = "book";
	}
	else
	{
		$post_type = "book_article";
	}

	$post_status = "publish";

	if(!$wsFound)
	{
		$post_status = "draft";
	}

	if($postID > 0)
	{
		wp_update_post(array('ID' => $postID,
		"post_type" => $post_type,
		 "post_title" => $data["title"],
		 "post_content" => $data["content"],
		 "post_status" => $post_status ,
		 ));
	}
	else
	{
			$postID = wp_insert_post(array("post_type" => $post_type,
		 "post_title" => $data["title"],
		 "post_content" => $data["content"],
		 "post_status" => $post_status ,
		 ));
	}



	foreach($data["extra"] as $k => $v)
	{
		__update_post_meta($postID,$k,$v);
	}
	if($data["image"] != "")
	{
		Generate_Featured_Image($data["image"],$postID);
	}
	
}


function __update_post_meta( $post_id, $field_name, $value = '' )
{
	if ( empty( $value ) OR ! $value )
	{
		delete_post_meta( $post_id, $field_name );
	}
	elseif ( ! get_post_meta( $post_id, $field_name ) )
	{
		add_post_meta( $post_id, $field_name, $value );
	}
	else
	{
		update_post_meta( $post_id, $field_name, $value );
	}
}

function Generate_Featured_Image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2= set_post_thumbnail( $post_id, $attach_id );
}

?>


