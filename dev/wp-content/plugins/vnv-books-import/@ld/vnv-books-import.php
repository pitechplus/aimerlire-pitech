<?php
/*
  Plugin Name: VNV Books Import
  Plugin URI: http://www.vnv.ch
  Description: Import books for aimelire.ch from an XLS file, a folder of picture and web service
  Version: 1.0
  Author: Sébastien Kapsopoulos and Fabien Zennaro
  Author URI: http://www.vnv.ch
  License: VNV249
  Text Domain: vnv-books-import
*/

 require(plugin_dir_path( __FILE__ ) ."lib/SimpleXLSX.php");
 require(plugin_dir_path( __FILE__ ) ."lib/VNVSoapPayot.php");

class Vnv_Books_Import{

  // Constructor
	function __construct() {

		ini_set('display_errors', '1');
		error_reporting(E_ALL);

		add_action( 'admin_menu', array( $this, 'wpa_add_menu' ));
		register_activation_hook( __FILE__, array( $this, 'wpa_install' ) );
		register_deactivation_hook( __FILE__, array( $this, 'wpa_uninstall' ) );
	}

	/*
	  * Actions perform at loading of admin menu
	  */
	function wpa_add_menu() {

		add_menu_page( 'Import Books', 'Import Books', 'manage_options', 'vnv-import-books-dashboard', array(
						  __CLASS__,
						 'wpa_dashboard'
						), 'dashicons-book-alt','2.2.9');

		/*add_submenu_page( 'vnv-import-books', 'Import Books simple' . ' Dashboard', ' Dashboard', 'manage_options', 'vnv-import-dashboard', array(
							  __CLASS__,
							 'wpa_page_file_path'
							));*/
	}

	/*
	 * Actions perform on loading of menu pages
	 */
	function wpa_dashboard() {
		include( plugin_dir_path( __FILE__ ) . "includes/dashboard.php");
	}

	/*
	 * Actions perform on activation of plugin
	 */
	function wpa_install() {



	}

	/*
	 * Actions perform on de-activation of plugin
	 */
	function wpa_uninstall() {



	}

}

new Vnv_Books_Import();
