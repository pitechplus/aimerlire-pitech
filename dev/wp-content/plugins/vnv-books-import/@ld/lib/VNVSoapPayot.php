<?php

class VNVSoapPayot
{
	private $username = '';
	private $password = '';

	public function getBookDetails($eanCode)
	{
		$urlSoap = 'https://mobile.payot.ch/AimerlireWS/AimerLireAccessHandler.svc?wsdl';

		$clientSOAP = new SoapClient($urlSoap);

		// print_r($clientSOAP->__getTypes());
		// print_r($clientSOAP->__getFunctions());

		$requestParams = array(
		    'Login' => $this->username,
		    'Password' => $this->password,
		    'Eancode' => $eanCode, 
		);

		try {
			$clientSOAP = new SoapClient($urlSoap);
			$soapResponse = $clientSOAP->GetProductDetail(array("request" => $requestParams));
			$returnArray = array();

			if(!$soapResponse->GetProductDetailResult->IsValid)
			{
				return array("error" => 'Wrong EAN code');
			}

			foreach ($soapResponse->GetProductDetailResult->Detail as $key => $value)
			{
				$returnArray[$key] = $value;
			}

			if(count($returnArray) > 0)
			{
				return $returnArray;
			}else
			{
				return array("error" => 'Wrong EAN code');
			}
		} catch (SoapFault $fault) {
			trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
		}
	}
}
	
?>
