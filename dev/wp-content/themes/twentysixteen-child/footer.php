<?php $getNum = get_option( 'page_on_front' ); ?>
			</div>
		</div> 
	</div><!-- div page -->
	<footer class="col-sm-12">
	<?php // if (is_single() || (is_page_template('page-archives.php') || is_page_template('page-dossiers.php') || is_page_template('page-books.php'))) : ?>
		<?php
		$page_id = get_queried_object_id();
		$pub_banniere = get_field('pub_banniere', $page_id);
		$pub_banniere_2 = get_field('pub_banniere_2', $page_id);
		$lien_pub = get_field('lien_pub', $page_id);

		// $imglink = false;
		// $link = false;
		// if (!is_front_page()) { 
		// if ($page_id % 7 == 0) {
			// $imglink = '/wp-content/uploads/Banniere-Payot-Rentree-Gallimard-compressor.jpg'; 
			// $link = 'http://www.gallimard.fr/rentreelitteraire';
		// }
		// else if ($page_id % 6 == 0) {
			// $imglink = '/wp-content/uploads/Banner_AIMER-LIRE_940-X-H-275-compressor.jpg';
			// $link = 'http://www.vigousse.ch/';
		// }
		// else if ($page_id % 5 == 0) {
			// $imglink = '/wp-content/uploads/FMB-JazzEtLettres-Web-940x275-aimerLireCh-compressor.jpg';
			// $link = 'http://fondationbodmer.ch/';
		// }
		// else if ($page_id % 4 == 0) {
			// $imglink = '/wp-content/uploads/Le_Petit_Robert_940x275-compressor.jpg';
			// $link = 'https://www.payot.ch/https://www.payot.ch/Detail/le_petit_robert_de_la_langue_francaise-alain_rey-9782321010609?cId=0';
		// }
		// if ($page_id % 3 == 0) {
			// $imglink = '/wp-content/uploads/Payot_Bilal_pub.jpg';
			// $link = 'https://www.payot.ch/Detail/bug-enki_bilal_-9782203105782';
		// }
		// else if ($page_id % 2 == 0) {
			// $imglink = '/wp-content/uploads/fondation-janmichalski_pub.jpg';
			// $link = 'http://www.fondation-janmichalski.com/';
		// }
		// else {
			// $imglink = '/wp-content/uploads/Editionsfavre_pub.jpg';
			// $link = 'http://www.editionsfavre.com/';
		// }
		
			if (!empty($pub_banniere)) {
			?>
			<div class="row">
				<div class="container">
					<hr />
					<p><!--Publicité--></p>
					<div class="<?php if (!empty($pub_banniere_2)) { echo "pub-slider"; } ?>">
						<?php 
						echo '<div><a href="' . $lien_pub. '" target="_blank"><img class="leban" src="' . $pub_banniere . '" alt="Publicité" /></a></div>';
						if (!empty($pub_banniere_2)) {
							echo '<div><a href="' . $lien_pub. '" target="_blank"><img class="leban" src="' . $pub_banniere_2 . '" alt="Publicité" /></a></div>';
						}
						?>
					</div>
					<hr />
				</div>
			</div>
			<?php 
			}
		?>
		<?php 
		// }
		/*
		?>
		<div id="bloc-footer-1" class="row">
			<p>Retrouvez les derniers numéros d’Aimer Lire</p>
			<div class="article-holder">
				<?php
					$books = new WP_Query(array(
						'post_type'		=> 'magazines',
						'order' => 'DESC',
					)
				);
				?>
			<div class="nouveautes-slider" id="footer-slider">
				<?php if ($books->have_posts()) : while ($books->have_posts()) : $books->the_post(); ?>
			
			<div class="entry">
				<div class="img">
					<?php
					if (has_post_thumbnail())
					{
						echo '<a class="group magazine-pop" target="_blank" href="http://aimerlire.gasser-media.io/viewer/' . get_field('id_externe') . '">' . get_the_post_thumbnail(null, 'full') . '</a>';
					}
					?>
				</div>
				
			</div>
			<?php endwhile; ?>
			<?php endif; ?>
			</div>
			<?php */?>

			
			</div>
		</div>
		<div id="bloc-footer-ad" class="row">
			<h3>Librairies</h3>
			<div class="article-holder ramasser">
			<hr />
				<?php
					$books = new WP_Query(array(
						'post_type'		=> 'adresses',
						'orderby' => array( 'post_date' => 'ASC' ),
						// 'orderby' => array( 'post_date' => 'DESC', 'menu_order' => 'ASC' ),
					)
				);
				$i = 1;
				?>

				<?php if ($books->have_posts()) : while ($books->have_posts()) : $books->the_post(); ?>
			<div class="col-sm-4 col-12">
			<h4 class="adresse-title"><?php echo get_the_title(); ?></h4>
			<p class="adresse">
				<?php the_content(); ?>
			</p>
			</div>

			<?php if ($i == 3) { echo '<hr />'; $i = 0;  } ?>
			<?php $i++; ?>
			<?php endwhile; ?>
			<?php endif; ?>
			
			</div>
			<div class="col-sm-12">
				<a href="javascript:;" id="openfooter">Dérouler la liste</a>
			</div>
		</div>
		<div id="bloc-footer-2" class="row">
			<p><i class="fa fa-envelope" aria-hidden="true"></i> Inscrivez-vous à notre newsletter</p>
			<form class="form-inline" method="POST" action="#">
				<input class="form-control" type="email" placeholder="Votre e-mail">
				<input class="btn-black" type="submit" value="S'inscrire">
			</form>
		</div>
		<div id="bloc-footer-3" class="row">
			<nav class="navbar navbar-default navbar-footer" role="navigation">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<!--<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-footer">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div> -->
					
					<?php
					$menu = wp_nav_menu( array(
							// 'menu'              => 'primary',
							'menu_id'			=> 'menu-footer',
							'echo'				=> false,
							'depth'             => 1,
							'container'         => 'div',
							'container_class'   => 'navbar-collapse nav-footer in collapse',
							'menu_class'        => 'nav navbar-nav navbar-center',
							'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
							'walker'            => new wp_bootstrap_navwalker())
						);
					$menu = explode("</ul></div>", $menu);
					// $menu[] = '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-999"><a title="Instagram" href="//www.instagram.com/explore/locations/569517392/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>';
					// $menu[] = '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-998"><a title="Twitter" href="//twitter.com/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>';
					$menu[] = '</ul></div>';
					
					echo implode($menu);
					?>
				</div>
			</nav>
		</div>
		<div id="bloc-footer-4" class="row">
			<p><a href="http://www.payot.ch" target="_blank">La livraison est gratuite en Suisse sur www.payot.ch !</a></p>
		</div>
		<div id="bloc-footer-5" class="row">
			<p><?php echo date('Y') ?> / Tous droits réservés</p>
		</div>
	<?php // endif; ?>
	</footer>
<script>
jQuery(window).scroll(function () {
    var iCurScrollPos = jQuery(this).scrollTop();
    if (iCurScrollPos >= 0 && iCurScrollPos <= 120) {
		jQuery('body').removeClass('scrollinging');
    } else {
		jQuery('body').addClass('scrollinging');
	}

});
jQuery('#openfooter').click(function () {
    if (jQuery('.article-holder.ramasser').hasClass('active')) {
		jQuery('.article-holder.ramasser').removeClass('active');
		jQuery('#openfooter').html('Dérouler la liste');
		
    } else {
		jQuery('.article-holder.ramasser').addClass('active');
		jQuery('#openfooter').html('Fermer la liste');
	}

});


jQuery('#footer-slider').slick({
			dots: true,
			infinite: false,
			autoplay: false,
			autoplaySpeed: 3000,
			// centerMode: true,
			slidesToShow: 4,
			// variableWidth: true,
			// variableHeight: true,
			responsive: [
			{
			  breakpoint: 1024, 
			  settings: {
				slidesToShow: 2,
			  }
			}, 
			{
			  breakpoint: 900,
			  settings: {
				slidesToShow: 1,
			  }
			}
			]
		});
		
			jQuery('.nouveautes-slider-3').slick({
			dots: true,
			infinite: true,
			autoplay: false,
			autoplaySpeed: 3000,
			// centerMode: true,
			slidesToShow: 3,
			// variableWidth: true,
			// variableHeight: true,
			responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 2,
			  }
			},
			{
			  breakpoint: 900,
			  settings: {
				slidesToShow: 1,
			  }
			}
			]
		});
		
		jQuery('.nouveautes-slider-4').slick({
			dots: true,
			infinite: true,
			autoplay: false,
			autoplaySpeed: 3000,
			// centerMode: true,
			slidesToShow: 4,
			// variableWidth: true,
			// variableHeight: true,
			responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 2,
			  }
			},
			{
			  breakpoint: 900,
			  settings: {
				slidesToShow: 1,
			  }
			}
			]
		});		
		
		jQuery('.pub-slider').slick({
			dots: false,
			nav: false,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 3000,
			// centerMode: true,
			slidesToShow: 1,
			// variableWidth: true,
			// variableHeight: true,
			
		});		

</script>
<?php wp_footer(); ?>

</body>
</html>
