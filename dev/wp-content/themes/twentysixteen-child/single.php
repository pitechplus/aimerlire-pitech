<?php
get_header();

$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));
$articleDate = get_the_date('F, Y');
?>
<div class="single">
	<div id="article-single" class="col-md-12">
	<?php while ( have_posts() ) : the_post();
		echo "
		<div class='article-edition hide'>
 			<span class='number-and-time'>Aimer lire N°. $getNum  $articleDate <span>
		</div>
		";

		$type = get_post_type();
		the_content();
		endwhile;
		wp_reset_query();?>
	</div>
</div>
<div class="clearfix"></div>
<?php if ($type == 'post') {
	$currentID = get_the_ID();
	$posts = new WP_Query(array(
			'post_type'		=> $type,
			'post__not_in'	=> array($currentID),
			'meta_key'		=> 'numero_magazine',
			'meta_value'	=> $getNum,
			'orderby'		=> 'date',
			'order'			=> 'DESC',
		)
	);
?>
<div class="container articles-slider">
	<hr />
	<p>Articles</p>
	<div class="col-md-10 col-md-offset-1">
		<div class="slider">
			<?php

			if ($posts->have_posts()) : while ($posts->have_posts()) : $posts->the_post();
				echo '<div class="slide">';
				echo '<h3>' . (strlen(get_the_title()) > 40 ? substr(get_the_title(), 0, 40) . "..." : get_the_title()) . '</h3>';
				echo '<a href="' . get_permalink() . '">Lire l\'article</a>';
				echo '</div>';
				endwhile;
				endif;
			?>
		</div>
	</div>
</div>
<?php } ?>


<div class="container">
	<div class="row">
		<hr>
		<p><!--Publicité--></p>
		<?php
		$rand = rand(1, 5);
		?>
		<div class="">
		<?php if($rand == 1) { ?>
			<div><a href="https://www.payot.ch/Dynamics/Result?query=Q2F0YWxvZz1GciZEYXRlRnJvbT0wMSUyZjA4JTJmMjAxNiswMCUzYTAwJTNhMDAmRGVzdGluYXRpb249cGF5b3QmRWRpdG9yPWhvbW1lJkl0ZW1zUGVyUGFnZT0xMCZMYW5ndWFnZT1mciZMaXN0Q29kZT1Ob0xpc3RTZWFyY2gmTmV3U2VhcmNoPVRydWUmT25seVN0cmljdFNlYXJjaD1GYWxzZSZQYWdlTnI9MSZTZWFyY2hUeXBlPUFkdmFuY2VkU2VhcmNoJlNob3dMb2c9RmFsc2UmU29ydD1Ob1NvcnQmU291cmNlPXd3dyZUaXRsZT1yJWMzJWE5aW52ZW50ZSZUaXRsZVNlYXJjaFR5cGU9MSZVbmlxdWVQYXJhbGxlbFNlYXJjaElkPTc1NjVmZDU2LWUzYjYtNGY4Mi04M2U4LTI0NjZmODUxYTc5Zg==" target="_blank"><img class="leban" src="/wp-content/uploads/Editionsdelhomme.jpg" alt="Publicité"></a></div>
		<?php } else if($rand == 2) { ?>
			<div><a href="http://www.gallimard.fr/" target="_blank"><img class="leban" src="/wp-content/uploads/Gallimard.jpg" alt="Publicité"></a></div>
		<?php } else if($rand == 3) { ?>
			<div><a href="https://www.auzou.ch/" target="_blank"><img class="leban" src="/wp-content/uploads/Auzou.jpg" alt="Publicité"></a></div>
		<?php } else if($rand == 4) { ?>
			<div><a href="http://www.loro.ch" target="_blank"><img class="leban" src="/wp-content/uploads/LoterieRomande.jpg" alt="Publicité"></a></div>
		<?php } else if($rand == 5) { ?>
			<div><a href="https://editions.flammarion.com/" target="_blank"><img class="leban" src="/wp-content/uploads/Flammarion.jpg" alt="Publicité"></a></div>
		<?php } else if($rand == 6) { ?>
			<div><a href="http://www.editionslibretto.fr/" target="_blank"><img class="leban" src="/wp-content/uploads/libella_07juillet_libretto-20-ans2.jpg" alt="Publicité"></a></div>
		<?php }  ?>

		</div>
		<hr>
	</div>

</div>

<?php get_footer(); ?>
<script>

	jQuery(document).ready(function(){
		jQuery('.slider').slick({
			dots: false,
			infinite: true,
			slide: '.slide',
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [{
				breakpoint: 991,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}]
		});

		// display share button on click (post)
		jQuery('#display-share-btn i').click(function(){

			var $btn = jQuery('#share-btn-article');
			if ($btn.is(':visible'))
				$btn.hide('slow');
			else
				$btn.show('slow');

		});

		$layerbook = jQuery('<div class="book-layer"><i class="fa fa-plus-circle"></i></div>');
		$layerbook.css({
			"background-color": "red",
		});

		if (jQuery(window).width() > 768) {
			jQuery('.book-article-div').hover(function(){
				jQuery(this).find("a.group").css("opacity", "0.3");
				var height = jQuery(this).height();
				var width = jQuery(this).width();

				$layerbook.css({
					"width": width + "px",
					"height": height + "px",
					"margin-top": height * -1 + "px",
					"line-height": height + "px",
					"vertical-align": "middle",
				});

				jQuery(this).append($layerbook);
			}, function() {
				jQuery(this).find("a.group").css("opacity", "1");
				jQuery(this).find('.book-layer').remove();
			});
		}

	});



</script>
