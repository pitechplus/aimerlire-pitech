<?php
/*
 * Template Name: Articles
 */
get_header();
?>

<div class="container articles">
	<h1>Les derniers articles</h1>
	<div class="entry">
		<?php the_content(); ?>
	</div>

	<?php

	$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));
	if (!$getNum)
		$getNum = 1;

	$args = array(
		'post_type' 	=> 'post',
		'posts_per_page' => -1,
		'orderby'	   	=> 'rand',
		'order'			  => 'DESC',
		'meta_key'		=> 'numero_magazine',
		'meta_value'	=> $getNum,
		'meta_query'  => array(
				array(
					'key' => '_thumbnail_id'
				)
		)
	);
	$query = new WP_Query($args);
	$cpt = 1;
	?>
	<div class="row nouveautes-slider multiple-items">
		<?
	if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();?>
		<?php
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		if (strpos($thumb_url[0], 'couvertureNonDisponible')) {
		} else {
		?>
		<div class="entry col-sm-6 articles">
			<div class="img">
				<?php
				if ( has_post_thumbnail() ) {
					echo '<a href="' . get_permalink() . '">', get_the_post_thumbnail( null, 'full' ) . '</a>' . share_btn_img( get_permalink(), false );
				}
				?>
			</div>
			<a href="<?php echo get_permalink(); ?>">
				<?php

				if ( get_field( 'type' ) )
					echo '<p class="type-content">' . get_field( 'type' ) . '</p>';

				the_title( '<h2 class="entry-title">', '</h2>' );
				?>
			</a>
			<p class="read-more"><a href="<?php echo get_permalink(); ?>">Lire l'article <i class="fa fa-eye" aria-hidden="true"></i></a>
			</p>
		</div>
		<?php

		}
		?>
		<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>
<div class="clearfix"></div>

<div id="article-page" class="container selections">

	<h1>Tous les articles</h1>

	<?php

	//
	// Filter by magazine number
	//
	$magazines = new WP_Query(array(
		'post_type'		=> 'magazines',
		'orderby'     => 'ID',
		'order' 			=> 'DESC'
		)
	);

	echo '<button id="btn-filter-master" class="btn-filter button-filter-master">Filtrer</button>';
	echo '<div class="categories"><div class="filters"><div class="tab">';
	 if ($magazines->have_posts()) : while ($magazines->have_posts()) : $magazines->the_post();
	 ?>
  	<button class="tablinks btn-filter button-filter <?php echo $magazines->post->ID ?>" onclick="sorting_magazines(event, '<?php echo $magazines->post->ID ?>')">Magazine <?php echo $magazines->post->post_title ?></button>

<?php endwhile; ?>
<?php endif; ?>

<?php echo '<hr /></div></div></div>';?>

<?php

//
// filter by categories@Alex
//

$args = array(
	'post_type' 	   => 'post',
	'posts_per_page' => -1,
	'orderby'	   	=> 'rand',
	'order'			  => 'DESC',
	'meta_key'		=> 'numero_magazine',
	'meta_value'	=> $getNum
	// 'meta_query'  => array(
	// 		array(
	// 			'key' => '_thumbnail_id'
	// 		)
	// )
);

$filterCat = "";
$query = new WP_Query($args);
echo '<div class="categories"><div class="cat-filters"><div class="tab">';
if ($query->have_posts()) :
	while ($query->have_posts()) : $query->the_post();
	$categories = get_field("type");
	$filterCat[] = str_replace(" ","-",$categories);

	?>
	<?php endwhile; ?>
	<?php
 	$uniq_filterCat = array_unique($filterCat);
	foreach($uniq_filterCat as $filter) { ?>
		<button id="<?php echo strtolower($filter); ?>" class="filter-cat filter-<?php echo strtolower($filter); ?>"> <?php echo $filter; ?> </button>
	<?php
	}
	?>
	<button id="reset-cat" class="all-cat filter-cat">Tout</button>

<?php endif; ?>

<?php echo '<hr /></div></div></div>';
//
// END filter by categories@Alex
//
?>

<?php

	$lastitem = json_decode(json_encode($magazines->post->ID), true);
	 if ($magazines->have_posts()) : while ($magazines->have_posts()) : $magazines->the_post();

	$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));
	$articles = new WP_Query( array(
		'post_type' 	=> 'post',
		'meta_key'		=> 'numero_magazine',
		'meta_value'	=> substr($magazines->post->post_name,2,1),
		'posts_per_page' => -1,
		'orderby' => 'rand',
		'order' => 'DESC'
		// 'meta_query' => array(
		// 		array(
		// 			'key' => '_thumbnail_id'
		// 		)
		// 	)
		)
		);

?>
<?php

$output ='<div id="'. $magazines->post->ID.'" class="article-holder selections articles tabcontent">';

		if ($articles->have_posts()) : while ($articles->have_posts()) : $articles->the_post();
			$cpt++;
			$thumb_id = get_post_thumbnail_id();
			$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		//if no cover
		if (strpos($thumb_url[0], 'couvertureNonDisponible')) {
		} else {

		$output .='<div class="col-sm-4 col-12 entry '. strtolower(get_field("type")) .'">
			<div class="img">';
				if (has_post_thumbnail()){
					$output .='<a class="group" href="' . get_permalink() . '">'.get_the_post_thumbnail(null, 'full') . '</a>';
				}

		$output.= share_btn_img(get_field('lien_dachat'), true);
		$output .='</div>';
		$output .='<div class="content-article">';
		$output .='<a href="'.get_permalink().'">';

		if (get_field('type'))
			$output .='<p class="type-content">' . get_field('type') . '</p>';
			//$output .='<p class="type-content-magazine">' . $magazines->post->post_title .' - '. strftime("%d %B %Y", strtotime($magazines->post->post_date)). '</p>';
			$output .='<p class="type-content-magazine">' . $magazines->post->post_title .' - '. strftime("%B %Y", strtotime($magazines->post->post_date)). '</p>';
		// if(strlen(get_the_title()) > 35){
		// 		$title = '<h2 class="entry-title">'.  substr(get_the_title(),0,35).'...</h2>';
		// 	}
		// 	else {
		//  $title =  '<h2 class="entry-title">'.  get_the_title() .'</h2>';
				$title = get_the_title();
		//	}

		$output .='<h2 class="entry-title">'.$title.'</h2><span class="entry-auteur">'.get_field('auteur').'</span>';

		$output .='</a>';
			$output .='</div>';
		$output .='<p class="read-more"><a href="'.get_permalink().'">Lire</a></p>';
		$output .='</div>';
		}
		endwhile;
		endif;
		$output .='</div>';

	// Reset post data
	wp_reset_postdata();

	echo $output;
?>
<?php endwhile; ?>
<?php endif; ?>

<?php
/*


function __update_post_meta( $post_id, $field_name, $value = '' )
{
	if ( empty( $value ) OR ! $value )
	{
		delete_post_meta( $post_id, $field_name );
	}
	elseif ( ! get_post_meta( $post_id, $field_name ) )
	{
		add_post_meta( $post_id, $field_name, $value );
	}
	else
	{
		update_post_meta( $post_id, $field_name, $value );
	}
}

?>

<div class="container archives">
	<h1>Catégories</h1>
	<div class="entry">

	</div>
	<?php





	$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));

	$articles = new WP_Query(array(
			'post_type'		=> 'post',
			'posts_per_page' => -1,
			'orderby' => 'ID',
			'order'   => 'desc',
			'meta_query' => array(
				array(
					'key' => 'numero_magazine',
					'value' => $getNum,
					'type' => 'numeric',
					'compare' => '<='
				),
				array(
					'key' => '_thumbnail_id'
				)
			)

		)
	);


//print_r($articles);

	$iCpt = 2;
	$arrayFilters = array();

	// $html is used to display content further
	// because we need to display btn choices before
	$html = '';

	$html .= '<div class="archives-content row">'; // open row container
	$html .= '	<div class="col-md-12 col-books">'; // open second col
	$html .= '		<h1>Les nouveautés</h1>';
	$html .= ''; // grid sizer for masonry

	if ($articles->have_posts()) : while ($articles->have_posts()) : $articles->the_post();

			$type = get_field('type');

			// Case insensitive search
			$typeLowercase = strtolower($type);
			$arrayFiltersLowercase = array_map('strtolower', $arrayFilters);

			if ($type && !in_array($typeLowercase, $arrayFiltersLowercase))
				$arrayFilters[] = $type;

			// if ($iCpt % 2 == 0)
				// $html .= '<div class="row">';

		if(strlen(get_the_title()) > 30){
				$title = '<h2 class="entry-title">'.  substr(get_the_title(),0,25).'...</h2>';
			}
			else {
				$title =  '<h2 class="entry-title">'.  get_the_title() .'</h2>';
			}

			$html .= '<div class="col-md-4 grid-item col-book">';
			$html .= '	<div id="' . get_field('ean') . '" class="entry anchor-ean-archives articles">';
			$html .= '		<div class="img">';
			$html .= 			(has_post_thumbnail() ? '<a class="group" href="#post-' . $id . '">' . get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img(get_field('lien_dachat'), true) : '');
			$html .= '		</div>';
			$html .= '		<a class="group" href="#post-' . $id . '">';
			$html .= 			($type ? '<p class="type-content">' . $type . '</p>' : '');
			$html .= $title;
			$html .= '		</a>';
			$html .= 		get_field('texte_magazine');
			$html .= '	</div>';
			$html .= '</div>';

			// display:none by default
			display_popup_book();

			// if ($iCpt % 2 != 0 || ($books->current_post +1) == ($books->post_count))
				// $html .= '</div>';

			//$iCpt++;

		endwhile;
	endif;

	$html .='		'; // close masonry grid
	$html .='	</div>'; // close second col

	$html .='</div>'; // close row

	wp_reset_postdata();

	// display filters

	// echo '<button id="btn-filter-master">Afficher les filtres</button>';
	echo '<div class="categories"><div class="filters">'; ?>

	<?php

	sort($arrayFilters);
	foreach($arrayFilters as $filter) {
		echo '<button class="btn-filter">' . $filter . '</button>';
	}
		echo '	<button class="btn-filter current">Tout afficher</button>';
	echo '<hr /></div></div>';

	// display content
	echo $html;
	*/
	?>
</div>
<div class="clearfix"></div>

<script type="application/javascript">
	jQuery(document).ready(function() {

		// display share button on click (articles)
		jQuery('.articles .share-btn i').each(function(){

			jQuery(this).click(function(){
				var $btn = jQuery(this).closest('.img').find('.share-btn-content');
				if ($btn.is(':visible'))
					$btn.hide('slow');
				else
					$btn.show('slow');
			});

		});
	});
</script>

<script>
	jQuery(document).ready(function(){

		$container = jQuery('.archives-content .grid');
		jQuery("#btn-filter-master").click(function(){
			if (jQuery(".categories").hasClass("open"))
				jQuery(".categories").removeClass("open");
			else
				jQuery(".categories").addClass("open");
		});

		jQuery(".tablinks").click(function(){
			if (jQuery(".categories").hasClass("open"))
				jQuery(".categories").removeClass("open");
			else
				jQuery(".categories").addClass("open");
		});

		jQuery(".filters > button").click(function(){

			var txt = jQuery(this).text().toLowerCase();
			jQuery(".filters .current").removeClass('current');
			jQuery(".grid-item").removeClass('grid-item');
			jQuery(this).addClass('current');

			var $content = jQuery('.archives-content .entry');
			$content.each(function(){
				if (jQuery('.type-content', this).text().toLowerCase() != txt && txt != "Tout afficher".toLowerCase()) {
					if (jQuery(this).parent().hasClass('tabcontent')) {
						jQuery(this).parent().hide();
						jQuery(".categories").removeClass("open");
					}
					else
						jQuery(this).hide();
					jQuery(".categories").removeClass("open");
				}
				else {
					if (jQuery(this).parent().hasClass('col-book')) {
						// if (!jQuery(this).parent().hasClass('grid-item'))
							jQuery(this).parent().addClass('grid-item');
						jQuery(".categories").removeClass("open");

						jQuery(this).parent().show();
					}
					else
						jQuery(this).show();
				}
			});

		});

	});
</script>



<?php get_footer(); ?>

<script type="application/javascript">


	jQuery( '.multiple-items' ).slick( {
		dots: true,
		infinite: true,
		slide: '.articles',
		autoplay: false,
		autoplaySpeed: 2000,
		slidesToShow: 2,
		slidesToScroll: 1,
		responsive: [ {
			breakpoint: 1024,
			settings: {
				slidesToShow: 2,
				slidesToScroll:1,
				infinite: true,
				dots: true
			}
		}, {
			breakpoint: 600,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}, {
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		} ]
	} );


	function sorting_magazines(evt, numero_magazine) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
		//tabcontent[i].style.display = "block";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace("current", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(numero_magazine).style.display = "block";
    evt.currentTarget.className += " current";
}

	//document.getElementById("<?php echo $lastitem;?>").click();

	jQuery(document).ready(function () {
    jQuery("#<?php echo $lastitem; ?>").show();
		jQuery(".<?php echo $lastitem; ?>").addClass('current');
		jQuery(".button.tablinks.btn-filter.button-filter.20123").addClass("a-class");
	});

	//@Alex get the param from the title
	//trigger click on the specific filter

	jQuery(window).on('load', function () {
		if (window.location.search.length > 1) {
				var artID = window.location.search.substr(1);
				var filterTab = document.getElementsByClassName(artID)[0];
				jQuery(".button-filter").each(function(){
					jQuery(this).removeClass("current");
				});
				filterTab.classList.add("current");
				filterTab.click();
				filterTab.focus();
		}
		//filter the magazine content hide and show the magazines
		//based on user selection

			jQuery(".filter-cat").on("click", function(){
				if(jQuery(this).hasClass("active")){
					jQuery(".filter-cat").each(function(){
						jQuery(this).removeClass("active");
					});
				}else {
					jQuery(".filter-cat").each(function(){
						jQuery(this).removeClass("active");
					});
					jQuery(this).addClass("active");
				}

				var theID = jQuery(this).attr("id");

				jQuery(".article-holder .entry").each(function(){
					jQuery(this).removeClass("hide");
				});

				jQuery(".article-holder .entry").each(function(){
					if(!jQuery(this).hasClass(theID)){
						jQuery(this).addClass("hide");
					}
				});
			});
			jQuery("#reset-cat").on("click", function(){
				jQuery(".article-holder .entry").each(function(){
					jQuery(this).removeClass("hide");
				});
			});

	});
</script>
