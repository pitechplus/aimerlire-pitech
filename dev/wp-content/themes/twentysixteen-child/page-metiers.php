<?php
/**
 *
 * Template Name: Metiers
 *
 */
get_header(); ?>
	<main id="main" class="" role="main">
		<div class="container-fluid">
			<section class="metiers-title-container">
				<div class="row">
					<h1> <?php the_title(); ?> </h1>
				</div>
			</section>
			<section class="metiers-blocks-container">
				<?php
				$metiers = new WP_Query( array(
					'post_type' 	   => 'nos_metiers',
					'posts_per_page' => 4,
					'orderby' 		   => 'rand',
					'order' 		     => 'DESC'
					)
				);

				if ($metiers->have_posts()) : while ($metiers->have_posts()) : $metiers->the_post();
					$authorName = get_field("metiers-author");
					$artDate = get_field("metiers-date");
					$i = 0;
				?>

					<div class="row metier-row">
						<div class="col-xs-12 col-sm-6 col-lg-6 metier-img-cont">
							<?php
							if(has_post_thumbnail()):
							  $thumb = the_post_thumbnail();
					 		endif;
							?>
							<img src="<?php echo $thumb; ?>" alt="">
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6 metier-content">
							<span class="metier-date"><?php echo $artDate; ?></span>
							<div class="metier-title">
								<h2><?php the_title(); ?></h2>
								<p class="metier-author"><span class="par">par</span> <?php echo $authorName; ?></p>
							</div>
							<div class="metier-text">
								<p><?php the_excerpt(); ?></p>
							</div>
							<div class="metier-button">
								<?php $i++;?>
								<a id="fancy-<?php echo $i;?>" class="fancy-metier" href="#post-<?php the_ID();?>">
									<button class="btn-lire" type="button" name="button">Lire d'avantage</button>
								</a>
								<?php display_popup_metier(); ?>
							</div>
						</div>
					</div>

				<?php endwhile;
				$i=0;
				?>

				<?php endif; ?>
			</section>
			<section class="row latest-magazines-section">
				<div class="col-lg-12 latest-mags-inner">
					<div class="row latest-mag-title">
						  <h2>Retrouver les derniers magazines: </h2>
					</div>
					<div class="row">
						<div class="magazines-slider col-xs-12 col-lg-12">
							<?php
								$magazines = new WP_Query(array(
									'post_type'		=> 'magazines',
									'order'       => 'DESC'
									)
								);
								if ($magazines->have_posts()) : while ($magazines->have_posts()) : $magazines->the_post();?>
									<div class="mag-issue">
										<?php the_post_thumbnail(); ?>
									</div>
								<?php endwhile; ?>
								<?php endif; ?>

						</div>
					</div>
				</div>
			</section>
			<div class="notice">
				<h3>La livraison est gratuite en Suisse</h3>
			</div>
		</div>
	</main><!-- .site-main -->
	<?php get_footer(); ?>

	<script>
	jQuery(document).ready(function(){

		jQuery(".fancy-metier", this).fancybox();

		jQuery(".btn-lire").on("click", function(){
			jQuery(".fancybox-overlay").load(function(){
				var imgH = jQuery(".pop-metier-img").height();
				console.log("height " + imgH);
				jQuery(".pop-metier-content").css("height", imgH);
			});
		});

		var docWidth = jQuery(document).width();
		if (docWidth > 992) {
			jQuery(".metier-row:nth-of-type(2n+1)").each(function(){
				jQuery(".metier-content", this).addClass("col-lg-pull-6");
				jQuery(".metier-img-cont", this).addClass("col-lg-push-6");
			});
		}

		jQuery('.magazines-slider').slick({
		  infinite: true,
		  slidesToShow: 3,
		  slidesToScroll: 3,
			responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
		});
	});


	</script>
