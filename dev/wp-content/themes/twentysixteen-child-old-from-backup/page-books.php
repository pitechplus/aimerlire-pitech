<?php
/*
*
* Template Name: Selection
*
*/
get_header();

// TO DO
// Get hash part of the URL -> check if exists
// If it doesn't exist, redirect to archives page with same hash

$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));

?>
<div class="container selections">
	<h1><?php the_title(); ?></h1>
	<div class="entry">
		<?php the_content(); ?>
	</div>

	<?php

	if (!$getNum) $getNum = 1;

	$args = array(
		'post_type' 		 => 'book',
		'meta_key'			 => 'numero_magazine',
		'meta_value'	   => $getNum,
		'posts_per_page' =>  5,
		'orderby'     	 => 'rand',
		'order' 			   => 'DESC',
		'meta_query' 		 => array(
				array(
					'key' => '_thumbnail_id'
				)
		)
	);
	$query = new WP_Query($args);
	//print_r($query);
	$cpt = 1;
	/*
	if ($query->have_posts()) {
		$tab = array(
			'Littérature' => array(),
			'Essais' => array(),
			'Polars et Science-Fiction' => array(),
			'Bande Dessinée' => array(),
			'Bien-être' => array(),
			'Jeunesse' => array(),
			'Arts et passions' => array()
		);

		foreach ($query->posts as $single_post) {
			$theme = get_field('type', $single_post->ID);

			if (isset($tab[$theme])) {
				$tab[$theme][] = (object)$single_post;
			}
		}

		$finalTab = array_merge(
			$tab['Littérature'],
			$tab['Essais'],
			$tab['Polars et Science-Fiction'],
			$tab['Bande Dessinée'],
			$tab['Bien-être'],
			$tab['Jeunesse'],
			$tab['Arts et passions']
		);
		$query->posts = $finalTab;
	}
	*/

	wp_reset_query();
	error_reporting(0);
		$cpt = 1;
	?>
	<div class="row nouveautes-slider multiple-items">
		<?
	if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();?>

		<?php
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		// echo $thumb_url[0];
		if (strpos($thumb_url[0], 'couvertureNonDisponible')) {
		} else {
		$id = get_the_ID();
		}

		?>
		<div id="<?php echo get_field('ean'); ?>" class="entry col-md-15 anchor-ean livres">
			<div class="img">
				<?php
				if (has_post_thumbnail()){
					echo '<a class="group" href="#post-' . $id . '">' . get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img(get_field('lien_dachat'), true);
				}
				?>
			</div>
			<a class="group" href="#post-<?php echo $id; ?>">
				<?php
				if (get_field('type'))
					echo '<p class="type-content">' . get_field('type') . '</p>';

			?>
			<?php
			// if(strlen(get_the_title()) > 20){
			// 	$title = '<h2 class="entry-title">'.  substr(get_the_title(),0,20).'...</h2>';
			// }
			// else {
			//$title =  '<h2 class="entry-title">'.  get_the_title() .'</h2>';
			$title = get_the_title();
			//}
			?>
			<h2 class="entry-title"><?php echo $title; ?></h2>

			</a>
			<?php
			// if(strlen(get_field('auteur')) > 25){
			// 	echo '<span class="entry-auteur">'.  substr(get_field('auteur'), 0, 25).'...</span>';
			// }
			// else {
				echo '<span class="entry-auteur">'.  get_field('auteur') .'</span>';
			//}

			?>

			<?php echo '<div class="txt-mag">' . substr(get_field('texte_magazine'), 0, 220) . '...</div>' ?>
			<a href="#post-<?php echo $id ?>" class="lire_btn group">Lire</a>
		</div>
		<?php display_popup_book() ?>

	<?php endwhile; ?>
	<?php endif;?>
</div>
</div>
<div class="clearfix"></div>

<?

function __update_post_meta( $post_id, $field_name, $value = '' )
{
	if ( empty( $value ) OR ! $value )
	{
		delete_post_meta( $post_id, $field_name );
	}
	elseif ( ! get_post_meta( $post_id, $field_name ) )
	{
		add_post_meta( $post_id, $field_name, $value );
	}
	else
	{
		update_post_meta( $post_id, $field_name, $value );
	}
}

?>

<div class="container archives livres">
	<h1 class="livres-title">Tous les livres chroniqués</h1>
	<h2 class="livres-subtitle">Bien plus qu'un simple résumé, retrouvez la chronique </br>
		de nos libraires pour chaque livre.</h2>
	<div class="entry">

	</div>
	<?php

	//@Alex
	$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));
	//$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$books = new WP_Query(array(
		'post_type'		   => 'book',
		'posts_per_page' => 20,
		//'orderby' 			 => 'ID',
		'meta_key'			 => 'numero_magazine',
		'orderby'     	 => 'rand',
		'order' 			   => 'DESC',
		'paged' 				 => $paged
		)
	);

	$iCpt = 2;

	$arrayFilters = array();
	$articleNoArr = array();
	// $html is used to display content further
	// because we need to display btn choices before
	$html = '';
	$html .= '<div class="archives-content row selections">'; // open row container
	$html .= '	<div class="col-md-12 col-books books1">'; // open second col
	$html .= ''; // grid sizer for masonry

	if ($books->have_posts()) : while ($books->have_posts()) : $books->the_post();

			$type = get_field('type');
			$articleNo = get_field('numero_magazine');
			// Case insensitive search
			$typeLowercase = strtolower($type);
			$arrayFiltersLowercase = array_map('strtolower', $arrayFilters);

			if ($type && !in_array($typeLowercase, $arrayFiltersLowercase))
				$arrayFilters[] = $type;

			if ($articleNo)
				$articleNoArr[] = $articleNo;

			// if ($iCpt % 2 == 0)
				// $html .= '<div class="row">';

			$title =  '<h2 class="entry-title">'.  get_the_title() .'</h2>';
			$author = '<span class="entry-auteur">'.  get_field('auteur') .'</span>';

			$html .= '<div class="col-md-15 grid-item col-book">';
			$html .= '	<div id="' . get_field('ean') . '" class="entry anchor-ean-archives livres">';
			$html .= '		<div class="img">';
			$html .= 			(has_post_thumbnail() ? '<a class="group" href="#post-' . $id . '">' . get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img(get_field('lien_dachat'), true) : '');
			$html .= '		</div>';
			$html .= '		<a class="group" href="#post-' . $id . '">';
			$html .= 			($type ? '<p class="type-content">' . $type . '</p>' : '');
			$html .=  $title;
		  $html .=  $author;
			$html .= '		</a>';
			//$html .= 		get_field('texte_magazine');
			$html .= '	</div>';
			$html .= '</div>';

			// display:none by default
			display_popup_book();

			// if ($iCpt % 2 != 0 || ($books->current_post +1) == ($books->post_count))
				// $html .= '</div>';
			$iCpt++;
		endwhile;
	endif;

	$html .='		'; // close masonry grid
	$html .='<a id="more_posts" href="#">Charger plus</a>';
	$html .='	</div>'; // close second col
	$html .='</div>'; // close row


	wp_reset_postdata();


	// display filters by magazine numbers
	echo '<button id="btn-filter-master" class="btn-filter button-filter-master">Filtrer</button>';
	echo '<div class="categories"><div class="books-mag-filters">';

	sort($articleNoArr);
	$uniqueArticleNo = array_unique($articleNoArr);

	$i= 0;
	foreach($uniqueArticleNo as $filterNo) {
		$i++;
		echo '<button id="magazine-no-'. $i .'" class="filter-mag btn-mag-no" data-mag-no="'.$filterNo.'">Magazine N° ' . $filterNo . '</button>';
	}
		echo '	<button id="reset-books" class="btn-filter btn-mag-no filter-mag button-filter current" data-mag-no="-1">Tout afficher</button>';
		echo '  </div></div>';

	// display filters by categories
	echo '<button id="btn-filter-master" class="btn-filter button-filter-master">Filtrer</button>';
	echo '<div class="categories"><div class="filters">'; ?>

	<?php

	sort($arrayFilters);
	$i= 0;
	foreach($arrayFilters as $filter) {
		$i++;
		echo '<button id="'. $i .'" class="btn-filter button-filter">' . $filter . '</button>';
	}
	echo '	<button class="btn-filter button-filter current">Tout afficher</button>';
	echo '<hr /></div></div>';

	// display content
	echo $html;

	?>


<?php
/*
function __update_post_meta( $post_id, $field_name, $value = '' )
{
	if ( empty( $value ) OR ! $value )
	{
		delete_post_meta( $post_id, $field_name );
	}
	elseif ( ! get_post_meta( $post_id, $field_name ) )
	{
		add_post_meta( $post_id, $field_name, $value );
	}
	else
	{
		update_post_meta( $post_id, $field_name, $value );
	}
}

?>

<div class="container archives">
	<h1>Catégories</h1>
	<div class="entry">

	</div>
	<?php

	$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$books = new WP_Query(array(
			'post_type'		=> 'book',
			'posts_per_page' => -1,
			'orderby' => 'ID',
			'order'   => 'desc',
		    'paged' => $paged,
			'meta_query' => array(
				array(
					'key' => 'numero_magazine',
					'value' => $getNum,
					'type' => 'numeric',
					'compare' => '<='
				),
				array(
					'key' => '_thumbnail_id'
				)
			)

		)
	);

	$iCpt = 2;
	$arrayFilters = array();
	// $html is used to display content further
	// because we need to display btn choices before
	$html = '';
	$html .= '<div class="archives-content row selections">'; // open row container
	$html .= '	<div class="col-md-12 col-books books1">'; // open second col
	$html .= ''; // grid sizer for masonry

	if ($books->have_posts()) : while ($books->have_posts()) : $books->the_post();
			$type = get_field('type');

			// Case insensitive search
			$typeLowercase = strtolower($type);
			$arrayFiltersLowercase = array_map('strtolower', $arrayFilters);

			if ($type && !in_array($typeLowercase, $arrayFiltersLowercase))
				$arrayFilters[] = $type;

			// if ($iCpt % 2 == 0)
				// $html .= '<div class="row">';


					if(strlen(get_the_title()) > 20){
				$title = '<h2 class="entry-title">'.  substr(get_the_title(),0,15).'...</h2>';
						$author = '<span class="entry-auteur">'.  substr(get_field('auteur'), 0, 25).'...</span>';
			}
			else {
				$title =  '<h2 class="entry-title">'.  get_the_title() .'</h2>';
				$author = '<span class="entry-auteur">'.  get_field('auteur') .'</span>';
			}

			$html .= '<div class="col-md-15 grid-item col-book">';
			$html .= '	<div id="' . get_field('ean') . '" class="entry anchor-ean-archives livres">';
			$html .= '		<div class="img">';
			$html .= 			(has_post_thumbnail() ? '<a class="group" href="#post-' . $id . '">' . get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img(get_field('lien_dachat'), true) : '');
			$html .= '		</div>';
			$html .= '		<a class="group" href="#post-' . $id . '">';
			$html .= 			($type ? '<p class="type-content">' . $type . '</p>' : '');
			$html .=  $title;
		    $html .= $author;
			$html .= '		</a>';
			//$html .= 		get_field('texte_magazine');
			$html .= '	</div>';
			$html .= '</div>';

			// display:none by default
			display_popup_book();

			// if ($iCpt % 2 != 0 || ($books->current_post +1) == ($books->post_count))
				// $html .= '</div>';

			$iCpt++;

		endwhile;
	endif;

	$html .='		'; // close masonry grid
	$html .='	</div>'; // close second col

	$html .='</div>'; // close row

	wp_reset_postdata();


	// display filters

	echo '<button id="btn-filter-master" class="btn-filter button-filter-master">Filtrer</button>';
	echo '<div class="categories"><div class="filters">'; ?>

	<?php

	sort($arrayFilters);
	$i= 0;
	foreach($arrayFilters as $filter) {
		$i++;
		echo '<button id="'. $i .'" class="btn-filter button-filter">' . $filter . '</button>';
	}
		echo '	<button class="btn-filter button-filter current">Tout afficher</button>';
	echo '<hr /></div></div>';
	//print_r($arrayFilters[0]);

	// display content
	echo $html;
*/
	?>
</div>
<div class="clearfix"></div>
 <?php //if(function_exists('load_more_button')) { load_more_button(); } ?>

<!--

 <script>
window.onload = function(){
  document.getElementById("1").click();
}
</script>
-->

<script>
	jQuery(document).ready(function(){

		var $container = jQuery('.archives-content .grid');

		jQuery("#btn-filter-master").click(function(){
			if (jQuery(".categories").hasClass("open"))
				jQuery(".categories").removeClass("open");
			else
				jQuery(".categories").addClass("open");
		});

		//Hidding the books on filter
		jQuery(".filters > button").click(function(){

			var txt = jQuery(this).text().toLowerCase();
			jQuery(".filters .current").removeClass('current');
			jQuery(".grid-item").removeClass('grid-item');
			jQuery(this).addClass('current');

			var $content = jQuery('.archives-content .entry');
			$content.each(function(){
				if (jQuery('.type-content', this).text().toLowerCase() != txt && txt != "Tout afficher".toLowerCase()) {
					if (jQuery(this).parent().hasClass('col-book')) {
						jQuery(this).parent().hide();
						jQuery(".categories").removeClass("open");
					}
					else
						jQuery(this).hide();
					  jQuery(".categories").removeClass("open");
				}
				else {
					//alert(txt)
					if (jQuery(this).parent().hasClass('col-book')) {
						// if (!jQuery(this).parent().hasClass('grid-item'))
						jQuery(this).parent().addClass('grid-item');
						jQuery(".categories").removeClass("open");
						jQuery(this).parent().show();
					}
					else
						jQuery(this).show();
				}
			});

		});

	});
</script>

<script type="application/javascript">
	jQuery(document).ready(function() {

		// display share button on click (books)
		jQuery('.selections .share-btn i').each(function(){

			jQuery(this).click(function(){
				var $btn = jQuery(this).closest('.img').find('.share-btn-content');
				if ($btn.is(':visible'))
					$btn.hide('slow');
				else
					$btn.show('slow');
			});

		});
	});
</script>


<script type="text/javascript">
	jQuery(document).ready(function() {

		jQuery('#columns > .row').totalScroll({
			speed: {selection: 1, article: 0.3},
			topOffset: '#header',
			singleColumn: "768px",
		});

		jQuery('.slider').slick({
			dots: true,
			infinite: true,
			slide: '.slide',
			autoplay: false,
			autoplaySpeed: 2000,
			// centerMode: true,
			// slidesToShow: 1,
			// slidesToScroll: 1,
			// variableWidth: true,
			// variableHeight: true,
		});

		jQuery("a.edito").fancybox({
      maxWidth    : 800,
      maxHeight   : 500,
      fitToView   : true,
      minWidth    : '50%',
      width       : '70%',
      // minHeight   : '70%',
      height      : '75%',
      autoSize    : true,
      closeClick  : false,
      openEffect  : 'none',
      closeEffect : 'none'
		});

		// display share button on click (content image)
		jQuery('#columns .entry .share-btn i').each(function(){
			jQuery(this).click(function(){
				var $btn = jQuery(this).closest('.img').find('.share-btn-content');
				if ($btn.is(':visible'))
					$btn.hide('slow');
				else
					$btn.show('slow');
			});

		});
	});

	jQuery('.multiple-items').slick({
	  dots: true,
		infinite: true,
		slide: '.livres',
		autoplay: false,
		autoplaySpeed: 2000,
	  slidesToShow: 5,
	  slidesToScroll: 1,
		responsive: [{
	            breakpoint: 1024,
	            settings: {
	                slidesToShow: 3,
	                slidesToScroll: 3,
	                infinite: true,
	                dots: true
	            }
	        }, {
	            breakpoint: 600,
	            settings: {
	                slidesToShow: 2,
	                slidesToScroll: 2
	            }
	        }, {
	            breakpoint: 480,
	            settings: {
	                slidesToShow: 2,
	                slidesToScroll: 1
	            }
	        }]
		});
		//Ajax calls for serving books and articles

    var ajaxUrl = "<?php echo admin_url('admin-ajax.php')?>",
    page = 1, // What page we are on.
    ppp = 20; // Post per page

		//Ajax for loading more books
		function appendBooks(){
			jQuery("#more_posts").on("click",function(e){ // When btn is pressed.
					e.preventDefault();
					jQuery("#more_posts").attr("disabled",true); // Disable the button, temp.
	        jQuery.post(ajaxUrl, {
	            action:"more_post_ajax",
	            offset: (page * ppp) + 1,
	            ppp: ppp,
	        }).success(function(posts){
							page++;
							jQuery(".col-books").append(posts);
							jQuery("#more_posts").attr("disabled",false);
					});
		   });
		}

		 //Ajax for filtering books based on magazine number@Alex
		 jQuery(".btn-mag-no").on("click",function(e){ // When btn is pressed.
 				e.preventDefault();
				var magazineNo = jQuery(this).data("mag-no");
				jQuery.post(ajaxUrl, {
           action: "get_mag_books",
					 magazineNo : magazineNo
         }).success(function(posts){
					 jQuery(".col-books").html(posts).append('<a id="more_posts" href="#">Charger plus</a>');
					 jQuery("#more_posts").on("click", function(e){
						 e.preventDefault();
					 });
					 appendBooks();
				});
 	   });

		 //Reset all books on page on button press
		 jQuery("#reset-books").on("click",function(e){ // When btn is pressed.
 				e.preventDefault();
				jQuery.post(ajaxUrl, {
           action:"reset_books"
				 }).success(function(posts){
					 jQuery(".col-books").html(posts).append('<a id="more_posts" href="#">Charger plus</a>');
					 jQuery("#more_posts").on("click", function(e){
						 e.preventDefault();
					 });
					 appendBooks();
				});
 	   });

		 //@Alex get the param from the title
		 //trigger click on the specific filter
		 jQuery(window).on('load', function () {
			//trigger click on button filter from URL when
			//redirected from the Magazines page
			if (window.location.search.length > 1) {
		 			var artID = window.location.search.substr(1);
					console.log("artID: " + artID);
					var filterTab = jQuery("#magazine-no-" + artID);
					console.log(filterTab);
		 			jQuery(".btn-mag-no").each(function(){
		 				jQuery(this).removeClass("current");
		 			});
		 			filterTab.addClass("current");
		 			filterTab.click();
					filterTab.focus();
		 	}

			//activate the category by which filter and filter
			jQuery(".filter-mag").on("click", function(){
				jQuery(".filter-mag").removeClass("active");
				jQuery(this).addClass("active");

				var theID = jQuery(this).attr("id");

				jQuery(".selections .entry").each(function(){
					jQuery(this).removeClass("hide");
				});

				jQuery(".selections .entry").each(function(){
					if(!jQuery(this).hasClass(theID)){
						jQuery(this).addClass("hide");
					}
				});
			});
			jQuery("#reset-cat").on("click", function(){
				jQuery(".selections .entry").each(function(){
					jQuery(this).removeClass("hide");
				});
			});
		 });
</script>

<?php get_footer(); ?>
