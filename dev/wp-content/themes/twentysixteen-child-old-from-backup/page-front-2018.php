<?php
/*
* Template Name: Accueil 2018
*/

get_header();
$cpt = 0;

// constant values for books and posts display loops
$MAX_BOOKS_PER_SCROLL = 17;
$MAX_POSTS_PER_SCROLL = 10;

// the_content();
$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));
$getTitle = get_field("titre_contenu", get_option( 'page_on_front' )) ? get_field("titre_contenu", get_option( 'page_on_front' )) : '';
$getSubTitle = get_field("sous_titre_contenu", get_option( 'page_on_front' )) ? get_field("sous_titre_contenu", get_option( 'page_on_front' )) : '';
if (!$getNum)
	$getNum = 1;

$books = new WP_Query(array(
		'post_type'		=> 'book',
		'meta_key'		=> 'numero_magazine',
		'meta_value'	=> $getNum,
	)
);

shuffle($books->posts);
$articles = new WP_Query(array(
		'post_type'		=> 'post',
		'orderby'		=> 'date',
		'order'			=> 'DESC',
		'meta_key'		=> 'numero_magazine',
		'meta_value'	=> $getNum,
	)
); ?>

<div class="container-fluid centered numeros-container">
	<div class="row">
		<div class="col-sm-4 col-8 col-sm-offset-4 col-offset-2">
			<h1 id="title-hp"><?php echo $getTitle ?></h1>
			<h2 id="subtitle-hp"><?php echo $getSubTitle; ?></h2>
		</div>
	</div>
</div>
<?php while ( have_posts() ) : the_post();

		the_content();
		endwhile;
		wp_reset_query();
	?>
<div class="clearfix"></div>

<?php get_footer(); ?>

<script type="text/javascript">
	jQuery(document).ready(function() {

		jQuery('.slider').slick({
			dots: true,
			infinite: true,
			slide: '.slide',
			autoplay: true,
			autoplaySpeed: 2000,
			// centerMode: true,
			// slidesToShow: 1,
			// slidesToScroll: 1,
			// variableWidth: true,
			// variableHeight: true,
		});
		jQuery("a.edito").fancybox({
            maxWidth    : 800,
            maxHeight   : 500,
            fitToView   : true,
            minWidth    : '50%',
            width       : '70%',
            // minHeight   : '70%',
            height      : '75%',
            autoSize    : true,
            closeClick  : false,
            openEffect  : 'none',
            closeEffect : 'none'
		});

			// display share button on click (content image)
		jQuery('.entry .share-btn i').each(function(){

		jQuery(this).click(function(){
			var $btn = jQuery(this).closest('.img').find('.share-btn-content');
			if ($btn.is(':visible'))
				$btn.hide('slow');
			else
				$btn.show('slow');
		});

	});
});
</script>
