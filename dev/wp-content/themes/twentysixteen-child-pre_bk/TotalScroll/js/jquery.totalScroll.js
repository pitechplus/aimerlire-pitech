/*
 * TotalScroll - Multi-column Layout Scrolling jQuery Plugin
 * http://plugins.gravitysign.com/total-scroll
 * Copyright (c) 2016 Roman Yurchuk
 * Version 1.0
*/


(function($, undefined) {
	
	"use strict";
	
	
	/**
		Global Variables 
	*/
	
	
	var container, columns, colspeed, primary, options;
		
		
	/**
		Function to get vendor specific CSS3 property name
	*/
		
	function getVendorProperty(name) {
		var prefix = ['Webkit','Moz','O','ms'];	
		var style = document.createElement('div').style;
		if(style[name] === '') {
			return name;
		} else {
			var ucname = name.charAt(0).toUpperCase() + name.substr(1);
			for(var p in prefix) {
				if(style[prefix[p] + ucname] === '') {
					return prefix[p] + ucname;
				}
			}
		}
		return undefined;
	}	
	
	
	/**
		Init
	*/
	
	function TotalScroll(){
	
		// setup columns
		setupColumns();
		
		// window resize handler
		$(window).on('resize.ts orientationchange.ts', function(){		
			
			// run setupColumns() as layout may have changed 
			setupColumns();
			
			
			// if screen width becomes less than "singleColumn" add skip class to all columns
			if($(window).width() < parseInt(options.singleColumn, 10)) {
				columns.addClass(options.skipClass);
			} else {
				columns.removeClass(options.skipClass);
			}
			
			// unstick columns & call layoutScroll
			$.each(columns, function(){
				unstickyColumn(this);
				layoutScroll();
			});
			
		});

		// check if iOS device
		var ios = navigator.userAgent.match(/(ipad|iphone)/i);
				
		// add scroll event listener
		$(window).on(ios ? "touchmove.ts" : "scroll.ts", layoutScroll);
		
	}
	
	
	/**
		Function to setup columns and populate global variables
		like "columns", "primary" and "colspeed"
	*/
	
	function setupColumns(){
	
		// get columns (except hidden and _copy)
		columns = container.children(options.selector).filter(':visible:not([id$=_copy])');		
				
		// reset style attribute to only have "position:relative"
		columns.attr('style', 'position: relative');
				
		// get primary column
		primary = columns.first();
		$.each(columns, function(){
			if($(this).outerHeight() > primary.outerHeight()) {
				primary = $(this);
			}
		});		
		
		// now create colspeed object
		colspeed = {};
		$.each(columns, function(){
		
			// skip column if #id attribute is not provided
			if(!this.id) { return; }
		
			// current column
			var column = $(this);
						
			// params to calculate min column speed
			var n = column.outerHeight() / primary.outerHeight();
			var m = $(window).height() / primary.outerHeight();
			
			// add entry for column
			colspeed[this.id] = {
				dom: this,
				speed: 1,
				min_speed: (n - m) / (1 - m),
				primary: (primary.get(0) === this)
			};
		
		});
		
		// check if "speed" plugin option was set
		if(typeof options.speed == "object") {
		
			// loop colspeed object and skip primary column
			for(var id in colspeed) {
				if(options.speed.hasOwnProperty(id) && !colspeed[id].primary) {
					if(options.speed[id] === 'auto') {
						colspeed[id].speed = colspeed[id].min_speed;
					} else if (options.speed[id] > 1 || options.speed[id] < 0) {
						colspeed[id].speed = 1;
					} else {
						colspeed[id].speed = Math.max(colspeed[id].min_speed, parseFloat(options.speed[id]));
					}
				}
			}
							
		}
			
	}
	
	
	/**
		Layout scroll event listener
	*/
	
	function layoutScroll(event){
	
		// get vendor transform property, e.g. WebkitTransform, MozTransform
		var transform = getVendorProperty('transform'); 
				
		// get scroll amount
		var top = document.documentElement.scrollTop || document.body.scrollTop;
			
		// now scroll columns 
		for(var k = 0; k < columns.length; k++) {

			// get column ID and DOM
			var id = columns[k].id, column = columns[k];
			
			// check if this column should be skipped
			var skip = $(column).hasClass(options.skipClass);
			
			// offset for non primary column
			var offset = 0;
						
			// column is not primary & is not skip column
			if( !colspeed[id].primary && !skip ) {
			
				// stick column to the top if it has "ts-fixed" class or is smaller as window's height
				if( $(column).hasClass(options.fixedClass) || $(column).outerHeight() < $(window).height() ) {
					stickyTop(column, top);
					continue;
				}			
				
				// check if we have reached topOffset
				if(top > getTopOffset()) {
								
					// calculate column offset based on its speed
					offset  = (1 - colspeed[id].speed) * (top - getTopOffset());
										
					// check if column should be fixed on bottom
					if( stickyBottom(column, top, offset) ) { 
						continue; 
					}
					
					// check if CSS3 should be used
					if(options.css3 && transform) {
						column.style[transform] = "translate3d(0," + Math.round(offset) + "px,0)";					
					} else { // fallback
						column.style.left = 0;
						column.style.top = Math.round(offset) + 'px';
					}
				
				}
			
			}
			
			// reveal content in column
			if( !skip ){
				scrollReveal(column, top, offset);
			}
			
		} // #scroll columns
		
		
		// call onScroll callback
		if(typeof options.onScroll === 'function') {
			options.onScroll.call(container.get(0), columns, top - getTopOffset());
		}
			
	}
	
	
	/** 
		Function to stick column at position (x,y) relative to viewport
	*/
	
	function stickyColumn(column, x, y) {
		if( !$(column).hasClass('ts-sticky') ) {
			
			// clone column and make it hidden
			$(column).clone().attr({
				style: 'visibility:hidden',
				id: column.id + '_copy'
			}).insertAfter(column);
			
			// add "ts-sticky" class to original column and make it fixed
			$(column).addClass('ts-sticky').removeAttr('style').css({
				position: 'fixed',
				left: x,
				top: y
			});
			
		}
	}
	
	
	/**
		Function to release sticky column and make it scroll with the page
	*/
	
	function unstickyColumn(column) {
		if( $(column).hasClass('ts-sticky') ) {
			container.find('#' + column.id +'_copy').remove();
			$(column).removeClass('ts-sticky').css({
				position: 'relative',
				top: 0,
				left: 0
			});
		}
	} 
	
	
	/**
		Function to stick column by bottom
	*/
	
	function stickyBottom(column, top, offset) {
				
		// calculate offset and on reaching it make column sticky
		var a = top + $(window).height();
		var b = getTopOffset() + $(column).outerHeight();
		
		if(a > b + offset) {
		
			var x = $(column).position().left,
				y = $(window).height() - $(column).outerHeight();
				
			// stick column at x,y position
			stickyColumn(column, x, y);
			
			// and return true
			return true;
			
		} else {
		
			unstickyColumn(column);
			
		}
	
	}
	
		
	/**
		Function to stick column by top
	*/
	
	function stickyTop(column, top) {
		if( top > getTopOffset() ) {
			var x = $(column).position().left, y = 0;
			stickyColumn(column, x, y);
		} else {
			unstickyColumn(column);
		}
	}
	
	
	/** 
		Function to get "topOffset" option 
	*/
	
	function getTopOffset() {
		switch(typeof options.topOffset) {			
			case "function":
				return parseInt(options.topOffset(), 10);
			case "string":
				return $(options.topOffset).outerHeight();
			default:
				return parseInt(options.topOffset, 10);
		}
	}
	
	
	/**
		Function to reveal content when it come into view
			
	*/
	
	function scrollReveal(column, top, offset) {
	
		// calculate reveal point (offset=0 for primary column)
		var p = (top + $(window).height()) - (getTopOffset() + offset);
		
		// check if reveal point has been reached
		$(options.revealSelector, column).each(function(){
			var y = $(this).position().top;
			if( y < p + (parseInt(options.revealOffset, 10) || 0) ) {
				$(this).addClass(options.revealClass);
			} else {
				if(options.revealReverse) {
					$(this).removeClass(options.revealClass);
				}
			}
		}); 

	}
	
	
	/**
		Define jQuery plugin
	*/
	
	$.fn.totalScroll = function(user_options){
			
		// init some global variables
		container = this;
		options = $.extend({
			css3: true,						// whether to use CSS3 translate3d() to shift columns
			speed: {},						// object in form {one:0.3, two:0.5, ..} where "one|two" are IDs of columns 
											// value 0-1 sets column speed relative to highest column
			selector: '',					// selector for columns inside of container, default none
			topOffset: 0,					// container offset from the top of the page (number, function or selector)
			skipClass: 'ts-skip',			// set this class on any column and TotalScroll will simply ignore it
			fixedClass: 'ts-fixed',			// set this class on any column to make it "fixed" at the top of page
			revealSelector: '.ts-reveal',	// selector for elements "to reveal" when page scrolls to them
			revealClass: 'ts-show',			// class to start "reveal" animation or transition (user defined)
			revealOffset: 0,				// pixel offset for reveal point, px
			revealReverse: false,			// if set to true, hide elements again when they move away
			singleColumn: "480px",			// screen width at which your grid collapses into single column, px
			onScroll: function(){},			// scroll callback (accepts 2 args - array with columns and scroll amount)
		}, user_options);
	
		// call init
		TotalScroll();
	};
	
})(jQuery);