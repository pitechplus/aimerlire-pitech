<?php
/**
* Template for Payot
*
*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="Expires" CONTENT="-1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo('name'); ?><?php /*if ( is_404() ) : ?> &raquo; <?php _e('Not Found') ?><?php elseif ( is_home() ) : ?> &raquo; <?php bloginfo('description') ?><?php else : ?><?php wp_title() ?><?php endif */?></title>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" media="screen" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>

	<script>
		jQuery(document).ready(function(){
		
			jQuery("a.group").fancybox({
				//'scrolling'   : 'no',
				'maxWidth': '1200px',
			});

			// hide #back-to-top first
			jQuery("#back-to-top").hide();
			
			// fade in #back-top
			jQuery(function () {
				jQuery(window).scroll(function () {
					if (jQuery(this).scrollTop() > 100) {
						jQuery('#back-to-top').fadeIn();
					} else {
						jQuery('#back-to-top').fadeOut();
					}
				});

				// scroll body to 0px on click
				jQuery('#back-to-top a').click(function () {
					jQuery('body,html').animate({
						scrollTop: 0
					}, 800);
					return false;
				});
			});
			
			// display share button on click (popup)
			jQuery('.book-share #display-share-btn i').click(function(){
				
				var $btn = jQuery('.book-share #share-btn-book');
				if ($btn.is(':visible'))
					$btn.hide('slow');
				else
					$btn.show('slow');
				
			});
			
			var anchor = window.location.hash;
			var regex = new RegExp('^#post-[0-9]+');
			if (regex.test(anchor)) {
				$link = jQuery('a[href="' + anchor + '"]');
				
				$link.click();
			}

		});
	</script>
</head>
<body id="body">
	<header id="header">
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-top">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo site_url(); ?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/brand-logo.jpg" alt="logo site" />
					</a>
					<div class="logo-top-over-img">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/AimerLire_logo.jpg" alt="logo" />
					</div>
				</div>
				
				<?php
				wp_nav_menu( array(
						'menu'              => 'primary',
						'depth'             => 1,
						'container'         => 'div',
						'container_class'   => 'navbar-collapse nav-top collapse',
						'menu_class'        => 'nav navbar-nav navbar-right',
						'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
						'walker'            => new wp_bootstrap_navwalker())
					);
				?>
			</div>
		</nav>
		<?php if (is_front_page()) : ?>
		<div class="header-hp">
			
			<div class="slider">
			<?php
				global $nggdb;
				if ($nggdb->find_gallery(1))
				{
					$gallery = $nggdb->get_gallery(1);
					$folder = "/wp-content/gallery/slider-accueil/";
					
					foreach ($gallery as $image)
					{
						echo '<div class="slide">'; 
						echo 	'<a href="' . $image->description . '"><img src="' . ($folder . $image->filename) . '" alt="image" /></a>';
						echo 	'<div class="caption-slider"><a href="' . $image->description . '"><p>' . $image->alttext . '</p></a></div>';
						echo 	'<a href="' . $image->description . '" class="btn btn-more">+</a>';
						echo'</div>';
					}
				}
			?>
				<div id="btn-slider-content">
					<a href="/les-articles">Découvrir les articles</a>
				</div>
			</div>
			<div class="text-header">
				<div class="container">
					<?php
						$arrayNews = array();
						
						$query = new WP_Query(array(
							'post_type'		=> 'news',
							'post_limit'	=> 3,
						));
						
						if ($query->have_posts()) {
							while($query->have_posts()) {
								$query->the_post();
								
								$arrayNews[] = get_post();
							}
						}
						
						wp_reset_postdata();
					?>
					<div class="actus">
						<p class="text-title">Actualités</p>
						<hr />
						<div class="row">
							<div class="col-md-4">
								<?php
									if (isset($arrayNews[0])) {
										if (has_post_thumbnail($arrayNews[0]->ID)) {
											echo get_the_post_thumbnail($arrayNews[0]->ID);
										}
									}
								?>
							</div>
							<div class="col-md-4">
								<?php
									if (isset($arrayNews[1])) {
										if (has_post_thumbnail($arrayNews[1]->ID)) {
											echo get_the_post_thumbnail($arrayNews[1]->ID);
										}
									}
								?>
							</div>
							<div class="col-md-4">
								<?php
									if (isset($arrayNews[2])) {
										if (has_post_thumbnail($arrayNews[2]->ID)) {
											echo get_the_post_thumbnail($arrayNews[2]->ID);
										}
									}
								?>
							</div>
						</div>
						<hr />
						<div class="row">
							<div class="col-md-4">
							<?php
								if (isset($arrayNews[0])) {
									echo '<h2>' . $arrayNews[0]->post_title . '</h2>';
									echo $arrayNews[0]->post_content;
								}
							?>
							</div>
							<div class="col-md-4">
							<?php
								if (isset($arrayNews[1])) {
									echo '<h2>' . $arrayNews[1]->post_title . '</h2>';
									echo $arrayNews[1]->post_content;
								}
							?>
							</div>
							<div class="col-md-4">
							<?php
								if (isset($arrayNews[2])) {
									echo '<h2>' . $arrayNews[2]->post_title . '</h2>';
									echo $arrayNews[2]->post_content;
								}
							?>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</header>
	<p id="back-to-top">
		<a href="#body">
			<i class="fa fa-long-arrow-up"></i>
		</a>	
	</p>

