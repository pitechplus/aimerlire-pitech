<?php
/*
*
* Template Name: Archives
*
*/
get_header();

// TO DO :
// Get hash (potentially) sent by page-books (how?)
// Verify EAN exists, else load the page normally


function __update_post_meta( $post_id, $field_name, $value = '' )
{
	if ( empty( $value ) OR ! $value )
	{
		delete_post_meta( $post_id, $field_name );
	}
	elseif ( ! get_post_meta( $post_id, $field_name ) )
	{
		add_post_meta( $post_id, $field_name, $value );
	}
	else
	{
		update_post_meta( $post_id, $field_name, $value );
	}
}

?>

<div class="container archives">
	<h1><?php the_title(); ?></h1>
	<div class="entry">
		<?php the_content(); ?>
	</div>
	<?php
	/*

	ini_set('display_errors', '1');
		error_reporting(E_ALL);

	$books = get_posts(array("post_type" => array("book", "book_article"),
		'numberposts' => -1,
		'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private')
		));


	foreach ($books as $book) {
		$nummag = get_post_meta($book->ID,"numero_magazine");
		if( count($nummag) <=  0)
		{
			var_dump(get_post_meta($book->ID,"numero_magazine"));
			__update_post_meta($book->ID,"numero_magazine","1");
			echo "lala";
		}
	}
		*/




	$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));

	$books = new WP_Query(array(
			'post_type'		=> 'book',
			'posts_per_page' => -1,
			'meta_query' => array(
				array(
					'key' => 'numero_magazine',
					'value' => $getNum,
					'type' => 'numeric',
					'compare' => '<'
				),
				array(
					'key' => '_thumbnail_id'
				)
			)

		)
	);

	// shuffle($books->posts);

	$iCpt = 2;

	$arrayFilters = array();

	// $html is used to display content further
	// because we need to display btn choices before
	$html = '';

	$html .= '<div class="archives-content row">'; // open row container
	$html .='	<div class="col-sm-6 col-sm-push-6">'; // open first col
	$html .= '		<h1>Les articles</h1>';

	wp_reset_postdata();

	$articles = new WP_Query(array(
			'post_type'		=> 'post',
			'orderby'		=> 'date',
			'order'			=> 'DESC',
			'meta_query' => array(
				array(
					'key' => 'numero_magazine',
					'value' => $getNum,
					'type' => 'numeric',
					'compare' => '<'
				)
			)
		)
	);

	if ($articles->have_posts()) : while ($articles->have_posts()) : $articles->the_post();

		$type = get_field('type');

		if ($type && !in_array($type, $arrayFilters))
			$arrayFilters[] = $type;

		$html .= '<div class="row">';
		$html .= '	<div class="col-sm-12">';
		$html .= '		<div class="entry">';
		$html .= '			<div class="img">';
		$html .= 				(has_post_thumbnail() ? '<a href="' . get_permalink() . '">' . get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img(get_permalink(), false) : '');
		$html .= '			</div>';
		$html .= '			<a href="' . get_permalink() . '">';
		$html .= 			 ($type ? '<p class="type-content">' . $type . '</p>' : '');
		$html .= '			<h2 class="entry-title">' . get_the_title() . '</h2>';
		$html .= '			</a>';
		$html .= '			<p class="read-more"><a href="<' . get_permalink() . '">Lire l\'article <i class="fa fa-eye" aria-hidden="true"></i></a></p>';
		$html .= '		</div>';
		$html .= '	</div>';
		$html .= '</div>';
		endwhile;
	endif;

	$html .='	</div>'; // close first col

	$html .= '	<div class="col-sm-6 col-books col-sm-pull-6">'; // open second col
	$html .= '		<h1>Les nouveautés</h1>';
	$html .= '		<div class="grid">'; // open masonry container
	$html .= '			<div class="grid-sizer col-xs-6"></div>'; // grid sizer for masonry

	if ($books->have_posts()) : while ($books->have_posts()) : $books->the_post();

			$type = get_field('type');

			// Case insensitive search
			$typeLowercase = strtolower($type);
			$arrayFiltersLowercase = array_map('strtolower', $arrayFilters);

			if ($type && !in_array($typeLowercase, $arrayFiltersLowercase))
				$arrayFilters[] = $type;

			// if ($iCpt % 2 == 0)
				// $html .= '<div class="row">';

			$html .= '<div class="col-xs-6 grid-item col-book">';
			$html .= '	<div id="' . get_field('ean') . '" class="entry anchor-ean-archives">';
			$html .= '		<div class="img">';
			$html .= 			(has_post_thumbnail() ? '<a class="group" href="#post-' . $id . '">' . get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img(get_field('lien_dachat')) : '');
			$html .= '		</div>';
			$html .= '		<a class="group" href="#post-' . $id . '">';
			$html .= 			($type ? '<p class="type-content">' . $type . '</p>' : '');
			$html .= '			<h2 class="entry-title">' . get_the_title() . '</h2>';
			$html .= '		</a>';
			$html .= 		get_field('texte_magazine');
			$html .= '	</div>';
			$html .= '</div>';

			// display:none by default
			display_popup_book();

			// if ($iCpt % 2 != 0 || ($books->current_post +1) == ($books->post_count))
				// $html .= '</div>';
			$iCpt++;
		endwhile;
	endif;

	$html .='		</div>'; // close masonry grid
	$html .='	</div>'; // close second col

	$html .='</div>'; // close row

	wp_reset_postdata();

	// display filters

	// echo '<button id="btn-filter-master">Afficher les filtres</button>';
	echo '<div class="filters">'; ?>

	<?php
	echo '	<button class="btn-filter current">Tout afficher</button>';
	foreach($arrayFilters as $filter) {
		echo '<button class="btn-filter">' . $filter . '</button>';
	}
	echo '<hr /></div>';

	// display content
	echo $html;

	?>
</div>
<div class="clearfix"></div>
<script>
	jQuery(document).ready(function(){

		$container = jQuery('.archives-content .grid');

		$container.imagesLoaded(function(){
			$container.masonry({
				itemSelector: '.grid-item',
				columnWidth: '.grid-sizer',
				percentPosition: true,
			});
		});

		jQuery("#btn-filter-master").click(function(){
			if (jQuery(".filters").hasClass("open"))
				jQuery(".filters").removeClass("open");
			else
				jQuery(".filters").addClass("open");
		});

		jQuery(".filters > button").click(function(){

			var txt = jQuery(this).text().toLowerCase();
			jQuery(".filters .current").removeClass('current');
			jQuery(".grid-item").removeClass('grid-item');
			jQuery(this).addClass('current');

			var $content = jQuery('.archives-content .entry');
			$content.each(function(){
				if (jQuery('.type-content', this).text().toLowerCase() != txt && txt != "Tout afficher".toLowerCase()) {
					if (jQuery(this).parent().hasClass('col-book')) {
						jQuery(this).parent().hide();
					}
					else
						jQuery(this).hide();
				}
				else {
					if (jQuery(this).parent().hasClass('col-book')) {
						// if (!jQuery(this).parent().hasClass('grid-item'))
							jQuery(this).parent().addClass('grid-item');

						jQuery(this).parent().show();
					}
					else
						jQuery(this).show();
				}
			});

			$container.masonry({
				itemSelector: '.grid-item',
				columnWidth: '.grid-sizer',
				percentPosition: true,
			});

		});

	});
</script>

<?php get_footer(); ?>
