<?php 
/*
* 
* Template Name: Accueil
* 
*/
get_header();
$cpt = 0;

// constant values for books and posts display loops
$MAX_BOOKS_PER_SCROLL = 17;
$MAX_POSTS_PER_SCROLL = 10;

?>

<?php
	$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));
	$getTitle = get_field("titre_contenu", get_option( 'page_on_front' )) ? get_field("titre_contenu", get_option( 'page_on_front' )) : '';
	$getSubTitle = get_field("sous_titre_contenu", get_option( 'page_on_front' )) ? get_field("sous_titre_contenu", get_option( 'page_on_front' )) : '';
	if (!$getNum)
		$getNum = 1;

	$books = new WP_Query(array(
			'post_type'		=> 'book',
			'meta_key'		=> 'numero_magazine',
			'meta_value'	=> $getNum,
		)
	);
	
	shuffle($books->posts);
	$articles = new WP_Query(array(
			'post_type'		=> 'post', 
			'orderby'		=> 'date',
			'order'			=> 'DESC',
			'meta_key'		=> 'numero_magazine',
			'meta_value'	=> $getNum,
		)
	);
?>
<div id="columns" class="container-fluid centered">
	<hr />
	<h1 id="title-hp"><?php echo $getTitle ?></h1>
	<h2 id="subtitle-hp"><?php echo $getSubTitle; ?></h2>
	<div class="row">
		<div id="selection" class="col-sm-4 col-sm-offset-2 <?php //col-sm-pull-5 ?>">
			<h1>Les nouveautés</h1>
			<?php if ($books->have_posts()) : while ($books->have_posts() && $cpt < $MAX_BOOKS_PER_SCROLL) : $books->the_post(); ?>
			<?php 
			$id = get_the_ID();
			$cpt++;
			?>
			<div class="entry">
				<div class="img">
					<?php
					if (has_post_thumbnail())
					{
						echo '<a class="group" href="#post-' . $id . '">' . get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img('post-' . get_the_ID());
					}
					?>
				</div>
				<a class="group" href="#post-<?php echo $id; ?>">
				<?php
					if (get_field('type'))
						echo '<p class="type-content">' . get_field('type') . '</p>';
					
					the_title('<h2 class="entry-title">', '</h2>');
					
				?>
				</a>
				<?php echo '<div class="txt-mag">' . get_field('texte_magazine') . '</div>' ?>
			</div>
			<?php display_popup_book(); ?>
			<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div id="article" class="col-sm-4 col-sm-offset-1 <?php // col-sm-push-6 ?>">
			<h1>Les articles</h1>
			<?php $cpt = 0; ?>
			<?php if ($articles->have_posts()) : while ($articles->have_posts() && $cpt < $MAX_POSTS_PER_SCROLL) : $articles->the_post();
			$cpt++; 
			?>
			<div class="entry">
				
				<div class="img">
					<?php
					if (has_post_thumbnail())
					{
						echo '<a href="' . get_permalink() . '">' , get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img(get_permalink(), false);
					}
					?>
				</div>
				<a href="<?php echo get_permalink(); ?>">
				<?php
				
				if (get_field('type'))
					echo '<p class="type-content">' . get_field('type') . '</p>';
				
					
					
				the_title('<h2 class="entry-title">', '</h2>');
				?>
				</a>
				<p class="read-more"><a href="<?php echo get_permalink(); ?>">Lire l'article <i class="fa fa-eye" aria-hidden="true"></i></a></p>
			</div>
			<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<?php get_footer(); ?>

<script type="text/javascript">
	jQuery(document).ready(function() {
		
		jQuery('#columns > .row').totalScroll({
			speed: {selection: 1, article: 0.3},
			topOffset: '#header',
			singleColumn: "768px",
		});
		
		jQuery('.slider').slick({
			dots: true,
			infinite: true,
			slide: '.slide',
			autoplay: false,
			autoplaySpeed: 2000,
			// centerMode: true,
			// slidesToShow: 1,
			// slidesToScroll: 1,
			// variableWidth: true,
			// variableHeight: true,
		});
		
		jQuery("a.edito").fancybox({
            maxWidth    : 800,
            maxHeight   : 500,
            fitToView   : true,
            minWidth    : '50%',
            width       : '70%',
            // minHeight   : '70%',
            height      : '75%',
            autoSize    : true,
            closeClick  : false,
            openEffect  : 'none',
            closeEffect : 'none'
		});
		
		// display share button on click (content image)
		jQuery('#columns .entry .share-btn i').each(function(){
			
			jQuery(this).click(function(){
				var $btn = jQuery(this).closest('.img').find('.share-btn-content');
				if ($btn.is(':visible'))
					$btn.hide('slow');
				else
					$btn.show('slow');
			});
			
		});
	});
</script>