<?php
/*
 * 
 * Template Name: Magazines
 * 
 */
get_header();

// TO DO
// Get hash part of the URL -> check if exists
// If it doesn't exist, redirect to archives page with same hash



?>

<div class="container selections">
	<h1>
		<?php the_title(); ?>
	</h1>
	<div class="entry">
		<?php the_content(); ?>
	</div>

	<?
		$magazines = new WP_Query(array(
			'post_type'		=> 'magazines',
			'order'         => 'DESC',
			)
		);
	
	
/*
	echo '<pre>';
	print_r($magazines);
	echo '</pre>';
*/
	?>


	<?php if ($magazines->have_posts()) : while ($magazines->have_posts()) : $magazines->the_post(); 
	$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));
	
	$books = new WP_Query( array(
		'post_type' 	=> 'book',
		'meta_key'		=> 'numero_magazine',
		'meta_value'	=> substr($magazines->post->post_name,2,1),
		'posts_per_page' => 7,
		'orderby' => 'ID',
		'order' => 'DESC',
		'meta_query' => array( 
				array(
					'key' => '_thumbnail_id'
				) 
		)
	)
		);
	
	
	$articles = new WP_Query( array(
		'post_type' 	=> 'post',
		'meta_key'		=> 'numero_magazine',
		'meta_value'	=> substr($magazines->post->post_name,2,1),
		'posts_per_page' => 3,
		'orderby' => 'ID',
		'order' => 'DESC',
		'meta_query' => array( 
				array(
					'key' => '_thumbnail_id'
				) 
		)
	)
		);
	
	?>
	<div class="row">
		<div class="col-md-4 magazine-cover">
			<?php

			if ( has_post_thumbnail() ) {
				echo '<a class="" target="_blank" href="http://aimerlire.gasser-media.io/viewer/' . get_field( 'id_externe' ) . '">' . get_the_post_thumbnail( null, 'full' ) . '</a>';
			}
			?>
		</div>
		<?php ?>

		<div class="col-md-8 magazine-content">
			<?php echo '<a class="btn-feuilleter" target="_blank" href="http://aimerlire.gasser-media.io/viewer/' . get_field('id_externe') . '" target="_blank">Feuilleter <br>le magazine</a>'; ?>
			<h2 class="entry-title" style="font-weight: bold"><?php echo $magazines->post->post_title; ?> - <?php echo date_i18n("F Y", strtotime($magazines->post->post_date))?> </h2>
			<h4>Livres recommandés dans ce numéro:</h4>
			<ol>
				<?php 
	
	
				foreach($books->posts as $post){
	
    		?>
				<li>
					<?php echo '<a class="group" href="#post-' . $post->ID . '">' . $post->post_title . '</a>'?>
				</li>
				<?
				
	display_popup_book();
				}?>
			</ol>
			<h4>Articles liés à ce numéro:</h4>
			<ul>
			 <?php if ($articles->have_posts()) : while ($articles->have_posts()) : $articles->the_post(); ?>
				<li>
					<a href="<?php echo get_permalink(); ?>" class="article-lie">
						<?php echo get_the_title(); ?>
					</a>
				</li>
				
				<?php endwhile; ?>
				<?php endif; ?>
			</ul>
		</div>
	</div>
	<?php endwhile; ?>
	<?php endif; ?>




</div>
<div class="clearfix"></div>



<?php get_footer(); ?>