<?php 
/*
* 
* Template Name: Articles
* 
*/
get_header();
?>

<div class="container articles">
	<h1><?php the_title(); ?></h1>
	<div class="entry">
		<?php the_content(); ?>
	</div>
	<?php 
	$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));
	if (!$getNum)
		$getNum = 1;
	
	$args = array(
		'post_type' 	=> 'post',
		'orderby'		=> 'date',
		'order'			=> 'DESC',
		'meta_key'		=> 'numero_magazine',
		'meta_value'	=> $getNum,
		'meta_query' => array( 
				array(
					'key' => '_thumbnail_id'
				) 
		)

	);
	$query = new WP_Query($args);
	$cpt = 1;
	
	if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();?>
		<?php 
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		if (strpos($thumb_url[0], 'couvertureNonDisponible')) {
		} else {
		$cpt++; 
		if ($cpt % 2 == 0)
			echo '<div class="row">';
		?>
		<div class="entry col-sm-6">
			<div class="img">
				<?php
				if (has_post_thumbnail())
				{
					echo '<a href="' . get_permalink() . '">' , get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img(get_permalink(), false);
				}
				?>
			</div>
			<a href="<?php echo get_permalink(); ?>">
			<?php
			
			if (get_field('type'))
				echo '<p class="type-content">' . get_field('type') . '</p>';
			
			the_title('<h2 class="entry-title">', '</h2>');
			?>
			</a>
			<p class="read-more"><a href="<?php echo get_permalink(); ?>">Lire l'article <i class="fa fa-eye" aria-hidden="true"></i></a></p>
		</div>
		<?php 
		if ($cpt % 2 != 0)
			echo '</div>';
		
		}
		?>
	<?php endwhile; ?>
	<?php endif; ?>
</div>
<div class="clearfix"></div>

<?php get_footer(); ?>

<script type="application/javascript">
	jQuery(document).ready(function() {
			
		// display share button on click (articles)
		jQuery('.articles .share-btn i').each(function(){
			
			jQuery(this).click(function(){
				var $btn = jQuery(this).closest('.img').find('.share-btn-content');
				if ($btn.is(':visible'))
					$btn.hide('slow');
				else
					$btn.show('slow');
			});
			
		});
	});
</script>