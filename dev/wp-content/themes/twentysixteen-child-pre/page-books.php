<?php 
/*
* 
* Template Name: Selection
* 
*/
get_header();

// TO DO
// Get hash part of the URL -> check if exists
// If it doesn't exist, redirect to archives page with same hash

$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));

?>

<div class="container selections">
	<h1><?php the_title(); ?></h1>
	<div class="entry">
		<?php the_content(); ?>
	</div>
	<?php 
	if (!$getNum)
		$getNum = 1;
	
	$args = array(
		'post_type' 	=> 'book',
		'meta_key'		=> 'numero_magazine',
		'meta_value'	=> $getNum,
		'meta_query' => array( 
				array(
					'key' => '_thumbnail_id'
				) 
		)
	);
	$query = new WP_Query($args);
	$cpt = 1;
	
	if ($query->have_posts()) {
		$tab = array(
			'Littérature' => array(),
			'Essais' => array(),
			'Polars et Science-Fiction' => array(),
			'Bande Dessinée' => array(),
			'Bien-être' => array(),
			'Jeunesse' => array(),
			'Arts et passions' => array()
		);
		
		foreach ($query->posts as $single_post) {
			$theme = get_field('type', $single_post->ID);
			
			if (isset($tab[$theme])) {
				$tab[$theme][] = (object)$single_post;
			}
		}
		
		$finalTab = array_merge(
			$tab['Littérature'],
			$tab['Essais'],
			$tab['Polars et Science-Fiction'],
			$tab['Bande Dessinée'],
			$tab['Bien-être'],
			$tab['Jeunesse'],
			$tab['Arts et passions']
		);
		
		
		$query->posts = $finalTab;
	}

	wp_reset_query(); 

	error_reporting(0);
	
		$cpt = 1;
	if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();?>
		<?php 
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		// echo $thumb_url[0];
		if (strpos($thumb_url[0], 'couvertureNonDisponible')) {
		} else {
		$id = get_the_ID();
		if ($cpt == 1) {
			echo '<div class="row">';
		}
		?>
		<div id="<?php echo get_field('ean'); ?>" class="entry col-xs-6 col-md-3 anchor-ean">
			<div class="img">
				<?php
				if (has_post_thumbnail())
				{
					echo '<a class="group" href="#post-' . $id . '">' . get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img(get_field('lien_dachat'), true);
				}
				?>
			</div>
			<a class="group" href="#post-<?php echo $id; ?>">
				<?php 
				if (get_field('type'))
					echo '<p class="type-content">' . get_field('type') . '</p>';
				 
				the_title('<h2 class="entry-title">', '</h2>');
				

				?>
				
			</a>
			<?php echo '<span class="entry-auteur">'.get_field('auteur').'</span>' ?>
			
			<?php echo '<div class="txt-mag">' . substr(get_field('texte_magazine'), 0, 9999) . '</div>' ?>
			<a href="#post-<?php echo $id ?>" class="lire_btn group">Lire</a>
		</div>
		<?php display_popup_book() ?>
		<?php 
		if ($cpt == 4) {
			echo '</div>';
			$cpt = 0;
		}
		$cpt++; 
		?>
	<?php } ?>
	
	<?php endwhile; ?>
	<?php endif; ?>
</div>
<div class="clearfix"></div>

<?php get_footer(); ?>

<script type="application/javascript">
	jQuery(document).ready(function() {
			
		// display share button on click (books)
		jQuery('.selections .share-btn i').each(function(){
			
			jQuery(this).click(function(){
				var $btn = jQuery(this).closest('.img').find('.share-btn-content');
				if ($btn.is(':visible'))
					$btn.hide('slow');
				else
					$btn.show('slow');
			});
			
		});
	});
</script>
