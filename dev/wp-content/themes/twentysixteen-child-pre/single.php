<?php 
get_header();

$getNum = get_field("numero_magazine", get_option( 'page_on_front' ));
?>
<div class="single">
	<div id="article-single" class="col-md-12">
	<?php while ( have_posts() ) : the_post();
	
		$type = get_post_type();
		the_content();
		endwhile;
		wp_reset_query();
	?>
	</div>
</div>
<div class="clearfix"></div>
<?php if ($type == 'post') { 

	$currentID = get_the_ID();
	$posts = new WP_Query(array(
			'post_type'		=> $type, 
			'post__not_in'	=> array($currentID),
			'meta_key'		=> 'numero_magazine',
			'meta_value'	=> $getNum,
			'orderby'		=> 'date',
			'order'			=> 'DESC',
		)
	);
?>
<div class="container articles-slider">
	<hr />
	<p>Articles</p>
	<div class="col-md-10 col-md-offset-1">
		<div class="slider">
			<?php
			
			if ($posts->have_posts()) : while ($posts->have_posts()) : $posts->the_post();
				echo '<div class="slide">';
				echo '<h3>' . (strlen(get_the_title()) > 40 ? substr(get_the_title(), 0, 40) . "..." : get_the_title()) . '</h3>';
				echo '<a href="' . get_permalink() . '">Lire l\'article</a>';
				echo '</div>';
				endwhile;
				endif;
			?>
		</div>
	</div>
</div>
<?php } ?>


<div class="row">
				<div class="container">
					<hr>
					<p><!--Publicité--></p>
					<?php
					$rand = rand(1, 5);
					?>
					<div class="">
					<?php if($rand == 1) { ?>
						<div><a href="https://www.payot.ch/Dynamics/Result?query=Q2F0YWxvZz1GciZEZXN0aW5hdGlvbj1wYXlvdCZFZGl0b3I9YXV6b3UmSXRlbXNQZXJQYWdlPTEwJkxhbmd1YWdlPWZyJkxpc3RDb2RlPU5vTGlzdFNlYXJjaCZOZXdTZWFyY2g9VHJ1ZSZPbmx5U3RyaWN0U2VhcmNoPUZhbHNlJlBhZ2VOcj0xJlNlYXJjaFR5cGU9QWR2YW5jZWRTZWFyY2gmU2hvd0xvZz1GYWxzZSZTb3J0PU5vU29ydCZTb3VyY2U9d3d3JlRpdGxlPWplK3Byb2dyZXNzZSZUaXRsZVNlYXJjaFR5cGU9MSZVbmlxdWVQYXJhbGxlbFNlYXJjaElkPTE1YTE1ZjY4LWIxYzEtNDM0YS04NWEzLWRjNjM4NTYzMDllYg%3d%3d&amp;PageTo=2" target="_blank"><img class="leban" src="/wp-content/uploads/BANNER-AUZOU-940x330.png" alt="Publicité"></a></div>	
					<?php } else if($rand == 2) { ?>
						<div><a href="" target="_blank"><img class="leban" src="/wp-content/uploads/Banner-pour-les-nuls.jpg" alt="Publicité"></a></div>
					<?php } else if($rand == 3) { ?>
						<div><a href="https://www.payot.ch/Dynamics/Result?acs=bW9udGVzc29yaSBuYXRoYW4=&c=0&rawSearch=montessori%20nathan.jpg" target="_blank"><img class="leban" src="/wp-content/uploads/Banner-bout-du-tunnel.jpg" alt="Publicité"></a></div>
					<?php } else if($rand == 4) { ?>
						<div><a href="http://www.loro.ch" target="_blank"><img class="leban" src="/wp-content/uploads/Banner-LoRo-Payot-Saxophone-web.jpg" alt="Publicité"></a></div>
					<?php } else if($rand == 5) { ?>
						<div><a href="http://www.payot.ch" target="_blank"><img class="leban" src="/wp-content/uploads/Annonce_Livraison-Gratuite_AimerLire_Site_2018_Image2-1.jpg" alt="Publicité"></a></div>
					<?php }  ?>
					
					</div>
					<hr>
				</div>
</div>
			

<?php get_footer(); ?>
<script>

	jQuery(document).ready(function(){
	
		
		jQuery('.slider').slick({
			dots: false,
			infinite: true,
			slide: '.slide',
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [{
				breakpoint: 991,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}]
		});
	
		// display share button on click (post)
		jQuery('#display-share-btn i').click(function(){
			
			var $btn = jQuery('#share-btn-article');
			if ($btn.is(':visible'))
				$btn.hide('slow');
			else
				$btn.show('slow');
			
		});
		
		$layerbook = jQuery('<div class="book-layer"><i class="fa fa-plus-circle"></i></div>');
		$layerbook.css({
			"background-color": "red",
		});
		
		if (jQuery(window).width() > 768) {
			jQuery('.book-article-div').hover(function(){
				jQuery(this).find("a.group").css("opacity", "0.3");
				var height = jQuery(this).height();
				var width = jQuery(this).width();
				
				$layerbook.css({
					"width": width + "px",
					"height": height + "px",
					"margin-top": height * -1 + "px",
					"line-height": height + "px",
					"vertical-align": "middle",
				});
				
				jQuery(this).append($layerbook);
			}, function() {
				jQuery(this).find("a.group").css("opacity", "1");
				jQuery(this).find('.book-layer').remove();
			});
		}
		
	});
	
	

</script>