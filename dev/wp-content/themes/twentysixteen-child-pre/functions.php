<?php

set_time_limit ( 9000 );
// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array(), null );
}

function theme_add() {
	wp_enqueue_style( 'font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css' );

	if ($_SERVER['REQUEST_URI'] != '/les-nouveautes') {
		wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js');
	}

	wp_enqueue_style( 'slick-css-single', get_stylesheet_directory_uri() . '/single.css');
	wp_enqueue_style( 'slick-css', get_stylesheet_directory_uri() . '/slick/slick/slick.css');
	wp_enqueue_style( 'slick-theme-css', get_stylesheet_directory_uri() . '/slick/slick/slick-theme.css');
	wp_enqueue_script( 'slick-js', get_stylesheet_directory_uri() . '/slick/slick/slick.min.js');
	wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/TotalScroll/bootstrap/css/bootstrap.min.css');
	wp_enqueue_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/TotalScroll/bootstrap/js/bootstrap.min.js');
	wp_enqueue_script( 'totalscroll', get_stylesheet_directory_uri() . '/TotalScroll/bootstrap/js/jquery.totalScroll.min.js');
	wp_enqueue_script( 'fancybox', get_stylesheet_directory_uri() . '/fancybox/source/jquery.fancybox.pack.js');
	wp_enqueue_style( 'fancybox-css', get_stylesheet_directory_uri() . '/fancybox/source/jquery.fancybox.css');

	if (is_page_template('page-archives.php')) {
		wp_enqueue_script('masonry-js', get_stylesheet_directory_uri() . '/js/masonry.pkgd.min.js');
		wp_enqueue_script('imagesLoaded', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.1.8/imagesloaded.pkgd.min.js');
	}
}

add_action('wp_enqueue_scripts', 'theme_add');

function wpdocs_excerpt_more( $more ) {
    return ' [...]';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more', 999 );

function wpdocs_custom_excerpt_length( $length ) {
    return 1500;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

// change default order in admin
function default_order_articles($query) {
	if ($query->get('post_type') == 'post') {
		// if ($query->get('orderby') == '') {
			// $query->set('orderby', 'ID');
			// // $query->set('meta_key', 'default_order_articles');
		// }
		// if ($query->get('order') == '')
			// $query->set('order', 'DESC');
	}

	return $query;
}
add_action('pre_get_posts', 'default_order_articles', 9);

// create id col in admin panel
function book_create_col_id($defaults) {

	$new = array();

	foreach($defaults as $key=>$value)
	{
		if ($key == 'title')
			$new['id'] = 'ID';

		$new[$key] = $value;
	}

	echo '<style>th#id {width: 80px;}</style>';

	return $new;
}

// display id col in admin panel
function book_display_col_id($col, $id) {
	if ($col == 'id')
		echo $id;
}

add_filter('manage_posts_columns', 'book_create_col_id');
add_filter('manage_posts_custom_column', 'book_display_col_id', 5, 2);

function get_the_excerpt_custom($post_id) {
	global $post;
	$save_post = $post;
	$post = get_post($post_id);
	$output = apply_filters('the_excerpt', $post->post_excerpt);
	$post = $save_post;
	return $output;
}


// create shortcode book
function book_display_in_article($attr) {
	global $post;


	$attr = shortcode_atts(
		array(
			'id' => 0,
		),
		$attr,
		'book'
	);

	$result = '';

	if (is_numeric($attr['id']))
	{
		$post = get_post($attr['id']);
		$book 	= get_post($attr['id'])->ID;



		$result .= '		<div class="entry book-article-div">                                                                                                                                                                                                      ';
		$result .= '			<a class="group" href="#post-' . $book . '">                                                                                                                                                               ';
		$result .= '				' . (has_post_thumbnail($book) ? get_the_post_thumbnail($book, 'full') : 'Image introuvable') . '                                                                                                                                                                                ';
		$result .= '			</a>                                                                                                                                                                                                                 ';
		// $result .= '			<p class="read-more"><a href="<?php echo get_permalink();">Lire l'article <i class="fa fa-eye" aria-hidden="true"></i></a></p>                                                                       ';
		$result .= '		</div>';

		ob_start();

		setup_postdata($post);

		display_popup_book();

		$result .= ob_get_clean();


	}
	else {
		$result = "ID du livre introuvable.";
	}

	return $result;
}

add_shortcode('book', 'book_display_in_article');

//add shortcode Auteur
function display_auteur_in_article() {
	return '<p id="auteur-article"><em>par</em> ' . strtoupper(get_field('auteur') ? get_field('auteur') : 'Anonyme') . '</p>';
}
add_shortcode('auteur', 'display_auteur_in_article');

function test()
{
	the_post_thumbnail();
}

function display_popup_book() {
	setlocale (LC_TIME, 'fr_CH.utf8','fra');

	?>
	<div id="post-<?php echo get_the_ID(); ?>" class="book-display book-popup" style="display: none;
    max-width: 1200px;">
		<div class="row">
			<div class="col-sm-3">
				<div class="col-sm-12 book-image popup-book-not-available" style="position:relative; padding-top: 20px; padding-bottom: 20px;">
				<?php
				if (has_post_thumbnail())
				{
					if(get_field('disponibilite') == 37)
					{
						echo '<img style="width: 80px; height: 80px; position: absolute; top: -20px; left:-20px;"
					  src="'.get_stylesheet_directory_uri().'/img/pastille-a-paraitre.png">';
					}

					the_post_thumbnail();
				}

				?>
				</div>
			</div>
			<div class="col-sm-9">
				<div class="row">
					<div class="col-sm-12 book-content">
					<?php
						echo '<p class="header-popup">';
						echo get_field('nom_serie');
						if (get_field('nom_serie') && get_field('tome'))
							echo ', ';
						echo get_field('tome');
						echo '</p>';

						the_title('<h3>', '</h3>');
						echo '<p class="subtitle">' . get_field('sous-titre') . '</p>';
					?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8 popup-book-author">
						<div class="popup-book-align-author">
						<?php
							echo '<p class="auteur">' . get_field('auteur') . '</p>';
							if (get_field('auteurs_secondaires'))
								echo '<p class="auteur-sec">' . get_field('auteurs_secondaires') . '</p>';

						?>
						</div>
					</div>
					<div class="col-sm-4 popup-book-share book-share">
						<?php share_btn(get_field('lien_dachat')); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8 popup-book-content">
						<div class="the-content">
							<?php the_content(); ?>
						</div>
					</div>
					<div class="col-sm-4 popup-book-details">

						<?php if (get_field('collection')) : ?>
						<p class="details-title">Collection:<br />
						<span><?php echo get_field('collection'); ?></span></p>

						<?php endif; if (get_field('edition')) : ?>
						<p class="details-title">Editeur<br /><span><?php echo get_field('edition'); ?></span></p>

						<?php endif; if (get_field('nombre_de_pages')) : ?>
						<p class="details-title">Nombre de pages<br /><span><?php echo get_field('nombre_de_pages'); ?></span></p>

						<?php endif; if (get_field('format')) : ?>
						<p class="details-title">Format<br /><span><?php echo get_field('format'); ?></span></p>

						<?php endif; if (get_field('date_de_parution')) : ?>
						<p class="details-title">Date de parution<br /><span><?php echo strftime("%B %Y", strtotime(get_field('date_de_parution'))); ?></span></p>

						<?php endif; if (get_field('matériel_accompagnement')) : ?>
						<p class="details-title">Matériel d'accompagnement<br /><span><?php echo get_field('matériel_accompagnement'); ?></span></p>

						<?php endif; ?>
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-sm-12 btn-book-div">

				<a class="btn-book" href="<?php echo get_field('extrait') ? get_field('extrait') : '" style="display: none;"'; ?>" target="_blank">Lire l'extrait</a>
				<a class="btn-book" href="<?php echo get_field('video') ? get_field('video') : '" style="display: none;"'; ?>" target="_blank">Vidéo</a>
				<a class="btn-book" href="<?php echo get_field('lien_dachat') ? get_field('lien_dachat') : '" style="display: none;"'; ?>" target="_blank">Acheter le livre<i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	<?php
}


//add shortcode Share btn
function display_share_btn_in_article($url="") {
	$html = '';

	$html .= '<div>';
	$html .= 	'<p id="display-share-btn"><i class="fa fa-share" aria-hidden="true"></i></p>';
	$html .= 	'<div id="share-btn-article" style="display:none">';
	if (!empty($url)) {
		$html .= 		do_shortcode('[addtoany url="' . $url . '"]');
	} else {
		$html .= 		do_shortcode('[addtoany]');
	}
	$html .=	'</div>';
	$html .=	'<p>Partager cet article</p>';
	$html .= '</div>';

	return $html;
}
add_shortcode('partager', 'display_share_btn_in_article');

function share_btn($anchor, $text=true) {
	echo get_share_btn($anchor, $text);
}

function get_share_btn($anchor, $text=true) {
	$html = '';

	$html .= '<div>';
	$html .= 	'<p id="display-share-btn">Partager ce livre <i class="fa fa-share" aria-hidden="true"></i></p>';
	$html .= 	'<div id="share-btn-book" style="display:none; position: absolute; top: -25px;">';
	// $html .= 		do_shortcode('[addtoany url="' . get_bloginfo('url') . '/les-nouveautes#' . $anchor . '"]');
	$html .= 		do_shortcode('[addtoany url="' . $anchor . '"]');
	$html .=	'</div>';
	$html .= '</div>';

	return $html;
}

function share_btn_img($linkname, $isSelection=true) {
	$html = '';
	// $url = ($isSelection ? get_bloginfo('url') . '/les-nouveautes#' . $linkname : $linkname);
	$url = $linkname;

	$html .= '<i class="share-btn">';
	$html .= 	'<i class="fa fa-share" aria-hidden="true"></i>';
	$html .= '</i>';
	$html .= 	'<div class="share-btn-content" style="display:none">';
	$html .= 		do_shortcode('[addtoany url="' . $url . '"]');
	$html .=	'</div>';

	return $html;
}

function myprefix_redirect_attachment_page() {
	if ( is_attachment() ) {
		global $post;
		if ( $post && $post->post_parent ) {
			wp_redirect( esc_url( get_permalink( $post->post_parent ) ), 301 );
			exit;
		} else {
			wp_redirect( esc_url( home_url( '/' ) ), 301 );
			exit;
		}
	}
}
add_action( 'template_redirect', 'myprefix_redirect_attachment_page' );

// Execute AJAX redirection if page is /les-nouveautes
function executeAjaxRequestBooks() {

	if ($_SERVER['REQUEST_URI'] == '/les-nouveautes') {
		?>
		<script type="text/javascript" src="http://aimerlire.ch/wp-includes/js/jquery/jquery.js?ver=1.12.4"></script>
		<script>
			(function(){
				var anchor = window.location.hash;
				anchor = anchor.substr(1, anchor.length);

				if (anchor != '' && jQuery(anchor).length <= 0) {
					jQuery.ajax({
						method:		'POST',
						url:		'<?php echo get_stylesheet_directory_uri(); ?>' + '/checkAnchor.php',
						data:		{ean: anchor},
						success:	function(data) {
							if (data == true) {
								window.location.href = '/les-archives#' + anchor;
							}
						},
						asyn:		false
					});
				}
			})();
		</script>
		<?php
	};
}
add_action('after_setup_theme', 'executeAjaxRequestBooks', PHP_INT_MAX);

// Add Shortcode
function recent_posts_articles( $atts , $content = null ) {
	$atts = shortcode_atts(
			array(
				'col' => '3',
				'offset' => '0',
				'posts' => '9',
			),
			$atts,
			'articles'
	);
	$getNum = get_field("numero_magazine", get_option( 'page_on_front' )) ?: 1;
	$articles = new WP_Query(array(
			'post_type'		=> 'post',
			'orderby'		=> 'date',
			'order'			=> 'DESC',
			'offset'			=> $atts['offset'],
			'posts_per_page'			=> $atts['posts'],
			'meta_key'		=> 'numero_magazine',
			'meta_value'	=> $getNum,
			'meta_query' => array(
				array(
					'key' => '_thumbnail_id'
				)
			)
		)
	);
	$col = 12 / $atts['col'];
	$cpt = 0;
	$output ='<div class="article-holder container selections ">';
		if ($articles->have_posts()) : while ($articles->have_posts()) : $articles->the_post();
			$cpt++;
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		if (strpos($thumb_url[0], 'couvertureNonDisponible')) {
		} else {
		$output .='<div class="col-sm-'.$col.' col-12 entry">

				<div class="img">';

					if (has_post_thumbnail())
					{
						$output .='<a href="' . get_permalink() . '">'.get_the_post_thumbnail(null, 'full') . '</a>';
					}

		$output .='</div>';
		$output .='<div class="img-txt">';
		$output .='<a href="'.get_permalink().'">';

				if (get_field('type'))
					$output .='<p class="type-content">' . get_field('type') . '</p>';

		$output .='<h2 class="entry-title">'.get_the_title().'</h2><span class="entry-auteur">'.get_field('auteur').'</span>';


		$output .='</a>';
		$output .='<p class="read-more"><a href="'.get_permalink().'">Lire</a></p>';
		$output .='</div>';
		$output .='</div>';
		}
		endwhile;
		endif;
		$output .='</div>';

	// Reset post data
	wp_reset_postdata();

	// Return code
	return $output;

}
add_shortcode( 'articles', 'recent_posts_articles' );

// Add Shortcode
function recent_nouveautes( $atts , $content = null ) {
	$atts = shortcode_atts(
			array(
				'posts' => '8',
				'ids' => '',
				'row' => '3',

			),
			$atts,
			'nouveautes'
	);
	$getNum = get_field("numero_magazine", get_option( 'page_on_front' )) ?: 1;
	// $ids = explode($atts['ids'], "1");



	$posts = explode(",", str_replace(" ", "", $atts['ids']));
	if (empty($atts['ids'])) {
			$books = new WP_Query(array(
					'post_type'		=> 'book',
					'orderby'   => 'rand',
					'posts_per_page' => $atts['posts'],
					'meta_key'		=> 'numero_magazine',
					'meta_value'	=> $getNum,
					'meta_query' => array(
						array(
							'key' => '_thumbnail_id'
						)
					)
				)
			);
	} else {
			$books = new WP_Query(array(
				'post_type'		=> 'book',
				'posts_per_page' => $atts['posts'],
				'post__in' => $posts,
				'meta_key'		=> 'numero_magazine',
				'meta_value'	=> $getNum,
				'meta_query' => array(
					array(
						'key' => '_thumbnail_id'
					)
				)
			)
		);
	}

	shuffle($books->posts);
	$cpt = 0;
	$output ='<div class="nouveautes-holder nouveautes-slider selections nouveautes-slider-'.$atts['row'].'">';
		if ($books->have_posts()) : while ($books->have_posts()) : $books->the_post();
	$id = get_the_ID();
			$cpt++;
			$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		if (strpos($thumb_url[0], 'couvertureNonDisponible')) {
		} else {


	$output .='<div class="entry">
				<div class="img">';
					if (has_post_thumbnail())
					{
						$output .='<a class="group" href="#post-' . $id . '">' . get_the_post_thumbnail(null, 'full') . '</a>';
					}
				$output .='</div>';

			if ($atts['row'] == 4) {
				$output .='<a class="group" href="#post-'.$id.'">';

					if (get_field('type'))
						$output .='<p class="type-content">' . get_field('type') . '</p>';

					$output .='<h2 class="entry-title">'.get_the_title().'</h2><span class="entry-auteur">'.get_field('auteur').'</span>';

				$output .='</a>';
				$output .='<p class="read-more"><a class="group" href="#post-'.$id.'">Lire</a></p>';
			}
			$output .='</div>';
			$output .=display_popup_book();
		}
		endwhile;
		endif;
		$output .='</div>';

	// Reset post data
	wp_reset_postdata();

	// Return code
	return $output;

}
add_shortcode( 'nouveautes', 'recent_nouveautes' );


// Add Shortcode
function recent_article_popup( $atts , $content = null ) {
	$atts = shortcode_atts(
			array(
				// 'posts' => '8',
				'ids' => '',
				// 'row' => '3',

			),
			$atts,
			'article_popup'
	);
	$getNum = get_field("numero_magazine", get_option( 'page_on_front' )) ?: 1;
	// $ids = explode($atts['ids'], "1");



	$posts = explode(",", str_replace(" ", "", $atts['ids']));
	$books = new WP_Query(array(
				'post_type'		=> 'book_article',
				'posts_per_page' => 30,
				'post__in' => $posts,
				'meta_key'		=> 'numero_magazine',
				'meta_value'	=> $getNum,
				'meta_query' => array(
					array(
						'key' => '_thumbnail_id'
					)
				)
			)
	);

	shuffle($books->posts);
	$cpt = 0;
	$output ='<div class="nouveautes-holder nouveautes-slider selections nouveautes-slider-3">';
		if ($books->have_posts()) : while ($books->have_posts()) : $books->the_post();
		$id = get_the_ID();
			$cpt++;
			$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		if (strpos($thumb_url[0], 'couvertureNonDisponible')) {
		} else {

	$output .='<div class="entry">
				<div class="img">';
					if (has_post_thumbnail())
					{
						$output .='<a class="group" href="#post-' . $id . '">' . get_the_post_thumbnail(null, 'full') . '</a>';
					}
				$output .='</div>';

			$output .='</div>';
			$output .=display_popup_book();
		}
		endwhile;
		endif;
		$output .='</div>';

	// Reset post data
	wp_reset_postdata();

	// Return code
	return $output;

}
add_shortcode( 'article_popup', 'recent_article_popup' );

add_filter( 'max_srcset_image_width', create_function( '', 'return 1;' ) );

add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});



function _get_all_meta_values($key, $type) {
    global $wpdb;
	$result = $wpdb->get_col(
		$wpdb->prepare( "
			SELECT DISTINCT pm.meta_value FROM {$wpdb->postmeta} pm
			LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
			WHERE pm.meta_key = '%s'
			AND p.post_status = 'publish' AND p.post_type = '{$type}'
			ORDER BY pm.meta_value",
			$key
		)
	);

	return $result;
}


// $Vnv_Books_Import = new Vnv_Books_Import();
$VNVSoapPayot = new VNVSoapPayot();

function process_book($row, $ajax=false){
	global $VNVSoapPayot;

		$current = array();
		$errors = array();
		$messages = array();
		$ean = $row[0];
		if(trim($ean) == "")
		{
			$rows_skipped++;
			return null;
		}

		$ean = str_replace("\xc2\xa0",'',$ean);

		$books = get_posts(array("post_type" => array("book", "book_article"),
	 	'meta_key' => 'ean',
	 	'meta_value' => $ean,
		'numberposts' => -1,
		'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private'),
		));

		$postID = 0;
		if(count($books) > 0)
		{
			$postID = $books[0]->ID;
			$current['id']=$postID;
		}

		$wsData = array();
		$time_start = microtime_float();
		$wsData = $VNVSoapPayot->getBookDetails($ean);
		
		 
/*
		echo '<pre>';
		print_r($wsData);
		echo '</pre>';
		exit();
*/
		$wsFound = true;

		if(isset($wsData["error"]))
		{

			$wsData["Title"] = "";
			$wsData["Summary"] = "";
			$wsData["Subtitle"] = "";
			$wsData["Authors"] = new stdClass();
			$wsData["Authors"]->Author = array();
			$wsData["Editor"] = "";
			$wsData["SerieNr"] ="";
			$wsData["PagesCount"] = "";
			$wsData["Format"] = "";
			$wsData["PublicationDate"] = "";
			$wsData["Collection"] = "";
			$wsData["Availability"] = 0;
			$wsData["YoutubeVideoURL"] = "";
			$wsData["Serie"] = "";

			$wsFound = false;

			$errors['EAN']= "<strong>Unknown EAN. </strong>";
		}




		$book["title"] = $wsData["Title"];
		$book["content"] = $wsData["Summary"];
		$book["extra"]["sous-titre"] = $wsData["Subtitle"];
		$book["extra"]["type"] = $row[2];
		$book["extra"]["auteur"]  = "";
		$book["extra"]["auteurs_secondaires"]  = "";

		if(!is_array($wsData["Authors"]->Author))
		{
			$wsData["Authors"]->Author = array($wsData["Authors"]->Author);
		}

		foreach ($wsData["Authors"]->Author as $author) {
			if($author->IsPrincipal == 1)
			{
				if($book["extra"]["auteur"] != "")
				{
					$book["extra"]["auteur"] .=", ". $author->Firstname ." ". $author->Lastname;
				} else {
					$book["extra"]["auteur"] .= $author->Firstname ." ". $author->Lastname;
				}

			}
			else
			{
				if($book["extra"]["auteurs_secondaires"] != "")
				{
					$book["extra"]["auteurs_secondaires"] .= ", ";
				}

				$fonction = '';
				if($author->Fonction != NULL)
				{
					$fonction = ' ('.$author->Fonction.')';
				}

				$book["extra"]["auteurs_secondaires"] .= $author->Firstname ." ". $author->Lastname . $fonction;
			}
		}

		$book["extra"]["edition"] = $wsData["Editor"];
		$book["extra"]["texte_magazine"] = $row[7];
		$book["extra"]["categorie"] = $row[3];
		$book["extra"]["extrait"] =  $row[6];
		$book["extra"]["ean"] = $ean;
		$book["extra"]["lien_dachat"] = "http://www.payot.ch/Detail/".$book["extra"]["ean"];
		$book["extra"]["nom_serie"] = $wsData["Serie"];
		$book["extra"]["tome"] = $wsData["SerieNr"];
		$book["extra"]["nombre_de_pages"] = $wsData["PagesCount"];
		$book["extra"]["format"] = $wsData["Format"];
		$book["extra"]["date_de_parution"] = $wsData["PublicationDate"];
		$book["extra"]["collection"] = $wsData["Collection"];
		$book["extra"]["numero_magazine"] = $magazinNo;
		$book["extra"]["video"] = $wsData["YoutubeVideoURL"];
		$book["extra"]["disponibilite"] = $wsData["Availability"];


		if($book["content"] == null)
		{
			$book["content"] = $book["extra"]["texte_magazine"];
		}

		if($book["title"] == null)
		{
			$book["title"] = $row[4];
		}

		$book["image"] = get_wp_installation()."/wp-content/uploads/import/images/".$row[0].".jpg";
		if(!file_exists($book["image"]))
		{
			$book["image"] = get_wp_installation()."/wp-content/uploads/import/images/couvertureNonDisponible.jpg";
			$errors['IMAGE']= "<strong>Image not found.</strong>";
		}


		$id=createPostFromArray($book, $row[1], $postID, $wsFound);

		$time_end = microtime_float();
		

		$time = round(($time_end - $time_start),3);
		if($ajax){
			$messages[] = '<tr>';
			if($id){
				//we had an existing item in our database, which means we updated it	
				$messages[] = '<td><span class="added-book">Added</span></td>';
			}elseif($current){
				$messages[] = '<td><span class="updated-book">Updated</span></td>';
			}
			$messages[] = '<td class="ean">'.$ean.'</td> ';
			$messages[] = '<td  class="book-title">'.$book["title"].'</td>, ';
			$messages[] = '<td class="book-author">'.$book["extra"]["auteur"].'</td>';
			$messages[] = '<td class="errors">';
			foreach ($errors as $err){
				$messages[] = $err;
			}	
			$messages[] = '</td>';	
			$messages[] = '<td class="time">'.$time.'s</td> ';
			$messages[] = '</tr>';
		}else{
			$messages[] = '<div class="book_process_result updated">';
			$messages[] = '<div class="book_info">';
			if($id){
				//we had an existing item in our database, which means we prolly updated it	
				$messages[] = '<span class="info"><span class="added-book">Added</span>: </span>';
			}elseif($current){
				$messages[] = '<span class="info"><span class="updated-book">Updated</span>: </span>';
			}
			$messages[] =  '<span class="ean">[EAN: '.$ean.' ]</span> ';
			$messages[] =  '<span class="title">'.$book["title"].'</span>, ';
			$messages[] =  '<span class="author">'.$book["extra"]["auteur"].'</span>';
			$messages[] = '</div>';			
			if($errors){
				$messages[] = '<div class="errors">';
				foreach ($errors as $err){
					$messages[] = $err;
				}
				$messages[] = '</div>';
			}		
			$messages[] = '</div>';
		}
			$messages = implode('',$messages);	
			if($ajax) {
				header('Content-type: application/json');
				echo json_encode(array($ean, $messages));
				wp_die();
			}
			elseif(!wp_doing_cron())
				echo $messages;	
	
}

 function handleXlSFile($file, $magazinNo,$ajax=false)
{
	
	

	$xlsx = new SimpleXLSX($file);
	$rows = $xlsx->rows();
	$rows_skipped=0;
	$current ;	$errors ; $messages ;
	if(empty($rows)){
		return;
	}
	
	if ($ajax) { 
		/* it's an AJAX call */ 
		$rows_json = json_encode($rows);
		header('Content-type: application/json');
		echo $rows_json;
		wp_die();
	}else{
		?>
	
	<?php
		foreach($rows as $row){
			
			process_book($row);
		}
	}

}

add_action('admin_head', 'aimer_custom_style');

function aimer_custom_style() {
  echo '<style>

 

  .added-book{color:#46b450; font-weight:bold;}
  .updated-book{color:orange; font-weight:bold;}

  .errors{
	  color:red;
  }

.book-import-info{
	font-size:1.3em;
	margin:20px 0;
}

.book-author{
	font-weight:normal;
	font-style:italic;
}
.book-title{
	font-weight:bold;
}
#cancel_import{visibility:hidden;}
#import-parameters{
	padding:10px; 
	display:inline-block; 
	margin:10px; 
	border: 1px solid #eee; 
	background:#fff; 
	vertical-align:top
}
#link-import-parameters{display:inline-block; margin:10px 0; vertical-align:top}
#import-parameters label {padding:5px;}
.has-tooltip{
position:relative;
}
.has-tooltip .tooltip{
	display:none; 
}
.has-tooltip:hover .tooltip{
	display:inline-block; 
	position:absolute;
	 width:120px;
	 padding:3px;
	top:100%;
	background:#555;
	color:#fff;
	font-size:0.9em;
	right:0;	
}

.time-remaining{
	margin:10px 0;
	padding:10px;
	display:inline-block; 
	border: 1px solid #eee; 
	background:#fff; 
	vertical-align:top;
}
  </style>';
}

add_action( 'admin_footer', 'my_action_javascript' ); // Write our JS below here

function my_action_javascript() { ?>
	<script type="text/javascript" >
	var booksToImport = [];

	var booksQueue = [];
	var maxQueue = 3;
	var throttle=0;
	var startTime;
	var booksCount = 0;
	
	jQuery(document).ready(function($) {

		$('#submit_import').on('click',function(ev){
			stopProcess = false;
			$('#results-table').hide();
			clearBooksTable();
			clearMessages();
			booksQueue = [];

			maxQueue = parseInt($('[name="queue_depth"]').val());
			throttle = parseInt($('[name="throttle"]').val())*1000;
			 

			$('.import-parameters-wrapper').hide();
			$('#import-parameters').css('visibility','hidden');	
			ev.preventDefault();
			$('#cancel_import').css('visibility','visible');
			displayMessage('<div class="info">Reading xlsx file...</div>', false);
			var data = {
			'action': 'process_xls',
			'magazine':$('[name="magazine"]').val()
			}
			$.post(ajaxurl,data,function(response){
				
				booksToImport = response;
				
				importBooks();
			});
			
		});

		$('#cancel_import').on('click',function(ev){
			ev.preventDefault();
			
			stopProcess = true;
			stopActions();
			$('#cancel_import').css('visibility','hidden');
			displayMessage('<div class="error">Stopped execution</div>',true);
			$('.import-parameters-wrapper').show();
			$('.time-remaining').hide();
			$('.time-remaining .time').html('computing...');
			
		});
		$('#link-import-parameters').on('click',function(e){
			e.preventDefault();
			$('#import-parameters').css('visibility','visible');			
		});
	});
	async function importBooks(){
						
		
		booksCount = booksToImport.length;
		
		displayMessage('<div class="info">File contains <strong>'+booksCount+
		'</strong> rows.</div><div class="book-import-info">Starting import, this will take a while.</div>', false);
		if(throttle)
		displayMessage('<div class="info">To prevent server overload, execution will be paused for <strong>'+(throttle/1000)+
		'</strong> seconds after each book.</div>', false);
		$('#results-table').show();
		$('.time-remaining').show();
		startTime =  new Date() / 1000;
		var action = null;
		for (var i = 0; i<booksCount; i++){
			

			var data = {
			'action': 'import_book',
			'book':booksToImport[i]
			}
			if (booksToImport[i][0]=='EAN') 
				continue; //skip header row.

			if(await waitQueue([booksToImport[i][0],null])){
				
			action =	$.post(ajaxurl,data,function(response){
					removeQueue(response[0]);
					displayBookImportResult(response[1]);
					calculateTimeRemaining(i);
				});
				assignAction(booksToImport[i][0],action);
			}
			else{
				break;
			}
		}
	}

	function sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}
	function stopActions(){
		var len = booksQueue.length
		for(var i = 0; i<len;i++){
			if(booksQueue[i][1])
			booksQueue[i][1].abort();
		}
	}

	function calculateTimeRemaining(currentBook){
		var remaining, hours, minutes, seconds=0;
		var response='';
		var booksRemaining = booksCount - currentBook;
		
		var elapsed = (new Date()/1000) - (startTime-throttle/1000);
		if (currentBook>1){
			remaining = (elapsed/currentBook) * booksRemaining;
			hours = Math.floor(remaining / 3600);
			minutes = Math.floor((remaining % 3600) / 60);
			seconds = Math.floor((remaining % 3600) % 60);	

			$('.time-remaining .time').html(hours+':'+minutes+':'+seconds);
		}
	}



	var stopProcess=false;
	function removeQueue(item){
		if(booksQueue instanceof Array ){
			booksQueue = booksQueue.filter(function(x) {
				return x[0] != item;
			});			
		}
	}
	function assignAction(ean, action){
		if(booksQueue instanceof Array ){
			booksQueue  = booksQueue.map(function(item) { return item[0] == ean ? [ean, action] : item; });
		}
	}


	async function waitQueue(item){
		
		var isBusy = true;
		while(isBusy){
			if (stopProcess) {				
				return false;
			}
			if(booksQueue.length>=maxQueue) {
				await sleep(100);
			}else {
				if(throttle!=0)
				await  sleep(throttle);
				booksQueue.push(item);
				isBusy=false;
				return true;
			}
		}
	}
	function displayBookImportResult(book){
		jQuery('#results-body').append(book);
	}
	function clearBooksTable(){
		jQuery('#results-body').html('');
	}
	
	function displayMessage(message, prepend){
	if(prepend)	jQuery('#import_result').prepend(message);
	else jQuery('#import_result').append(message);
	}
	function clearMessages(){
		jQuery('#import_result').html('');
	}

	</script> <?php
}
// Same handler function...
add_action( 'wp_ajax_process_xls', 'ajax_process_xls' );
function ajax_process_xls() {
	
	if(!is_admin())
		wp_die();
	$magazine = intval( $_POST['magazine'] );

	if(is_int($magazine) && $magazine>0 && $magazine<8){
		handleXlSFile(WP_CONTENT_DIR."/uploads/import/Aimerlire_Magazine{$magazine}.xlsx",$magazine,true);
	}	

	wp_die();
}
function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

// Same handler function...
add_action( 'wp_ajax_import_book', 'ajax_import_book' );
function ajax_import_book() {
	
	if(!is_admin())
		wp_die();
	$row = $_POST['book'];

	if(is_array($row)){
		echo process_book($row, $ajax=true);
	}
	else{
		echo 'error parsing input data';
	}

	wp_die();
}


function createPostFromArray($data,$new,$postID,$wsFound) // $postID != 0 = update
{

	//echo "created";
	if($new == 1)
	{
		$post_type = "book";
	}
	else
	{
		$post_type = "book_article";
	}

	$post_status = "publish";

	if(!$wsFound)
	{
		$post_status = "draft";
	}
	$new_pid = false;
	if($postID > 0)
	{
		wp_update_post(array('ID' => $postID,
		"post_type" => $post_type,
		 "post_title" => $data["title"],
		 "post_content" => $data["content"],
		 "post_status" => $post_status ,
		 ));
	}
	else
	{
			$new_pid = true;
			$postID = wp_insert_post(array("post_type" => $post_type,
		 "post_title" => $data["title"],
		 "post_content" => $data["content"],
		 "post_status" => $post_status ,
		 ));
	}



	foreach($data["extra"] as $k => $v)
	{
		__update_post_meta($postID,$k,$v);
	}
	if($data["image"] != "")
	{
		Generate_Featured_Image($data["image"],$postID);
	}
	if($new_pid) return $postID;

}


function __update_post_meta( $post_id, $field_name, $value = '' )
{
	if ( empty( $value ) OR ! $value )
	{
		delete_post_meta( $post_id, $field_name );
	}
	elseif ( ! get_post_meta( $post_id, $field_name ) )
	{
		add_post_meta( $post_id, $field_name, $value );
	}
	else
	{
		update_post_meta( $post_id, $field_name, $value );
	}
}

function Generate_Featured_Image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2= set_post_thumbnail( $post_id, $attach_id );
}



function get_wp_installation()
{
    $full_path = getcwd();
    $ar = explode("wp-", $full_path);
    return $ar[0];
}


function runMagCronJob1 () {
    handleXlSFile(get_wp_installation()."/wp-content/uploads/import/Aimerlire_Magazine1.xlsx",1);
} add_action('runMagCronJob1', 'runMagCronJob1');


function runMagCronJob2 () {
    handleXlSFile(get_wp_installation()."/wp-content/uploads/import/Aimerlire_Magazine2.xlsx",2);
} add_action('runMagCronJob2', 'runMagCronJob2');

function runMagCronJob3 () {
    handleXlSFile(get_wp_installation()."/wp-content/uploads/import/Aimerlire_Magazine3.xlsx",3);
} add_action('runMagCronJob3', 'runMagCronJob3');

function runMagCronJob4 () {
    handleXlSFile(get_wp_installation()."/wp-content/uploads/import/Aimerlire_Magazine4.xlsx",4);
} add_action('runMagCronJob4', 'runMagCronJob4');

function runMagCronJob5 () {
    handleXlSFile(get_wp_installation()."/wp-content/uploads/import/Aimerlire_Magazine5.xlsx",5);
} add_action('runMagCronJob5', 'runMagCronJob5');

function runMagCronJob6 () {
    handleXlSFile(get_wp_installation()."/wp-content/uploads/import/Aimerlire_Magazine6.xlsx",6);
} add_action('runMagCronJob6', 'runMagCronJob6');
function runMagCronJob7 () {
    handleXlSFile(get_wp_installation()."/wp-content/uploads/import/Aimerlire_Magazine7.xlsx",7);
} add_action('runMagCronJob7', 'runMagCronJob7');
function runMagCronJob8 () {
    handleXlSFile(get_wp_installation()."/wp-content/uploads/import/Aimerlire_Magazine8.xlsx",8);
} add_action('runMagCronJob8', 'runMagCronJob8');



//@Alex
//adding more books on the books page, on load more books button press
function more_post_ajax(){
    $offset = $_POST["offset"];
    $ppp = $_POST["ppp"];
    header("Content-Type: text/html");
    $args = array(
        'post_type' => 'book',
        'posts_per_page' => $ppp,
        'offset' => $offset,
    );

		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		$title = get_the_title();

		// if (strpos($thumb_url[0], 'couvertureNonDisponible')) {
		// } else {
			//$id = get_the_ID();
		//}
    $loop = new WP_Query($args);
    while ($loop->have_posts()) { $loop->the_post();
			$eanID = get_field('ean');
			$id = get_the_ID();

			$title =  '<h2 class="entry-title">'.  get_the_title() .'</h2>';
			$author = '<span class="entry-auteur">'.  get_field('auteur') .'</span>';
			$type = get_field('type');
			$html = '<div class="col-md-15 grid-item col-book">';
			$html .= '	<div id="' . get_field('ean') . '" class="entry anchor-ean-archives livres">';
			$html .= '		<div class="img">';
			$html .= 			(has_post_thumbnail() ? '<a class="group" href="#post-' . $id . '">' . get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img(get_field('lien_dachat'), true) : '');
			$html .= '		</div>';
			$html .= '		<a class="group" href="#post-' . $id . '">';
			$html .= 			($type ? '<p class="type-content">' . $type . '</p>' : '');
			$html .=  $title;
			$html .=  $author;
			$html .= '		</a>';
			//$html .= 		get_field('texte_magazine');
			$html .= '	</div>';
			$html .= '</div>';
			display_popup_book();
			echo $html;
		}
    exit;
}
add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
add_action('wp_ajax_more_post_ajax', 'more_post_ajax');

//Filtering the books with ajax, by their magazine number, on the books page
function get_mag_books(){
	header("Content-Type: text/html");
	$magNo = $_POST["magazineNo"];
	if ($magNo == '-1'){
		$args = array(
				'post_type' 	   => 'book',
				'posts_per_page' => 20
		);
	}else {
		$args = array(
				'post_type' 	   => 'book',
				'posts_per_page' => 20,
				'meta_query'=> array(
	          array(
	              //'key' => 'karma',
	              'compare' => '=',
	              'value' => $magNo
	              //'type' => 'numeric'
	           )
	        )
		);
	}

	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
	$title = get_the_title();

	//if (strpos($thumb_url[0], 'couvertureNonDisponible')) {
	//} else {

	//}
	$loop = new WP_Query($args);
	while ($loop->have_posts()) { $loop->the_post();
		$eanID = get_field('ean');
		$id = get_the_ID();

		$title =  '<h2 class="entry-title">'.  get_the_title() .'</h2>';
		$author = '<span class="entry-auteur">'.  get_field('auteur') .'</span>';
		$type = get_field('type');
		$html = '<div class="col-md-15 grid-item col-book">';
		$html .= '	<div id="' . $eanID . '" class="entry anchor-ean-archives livres">';
		$html .= '		<div class="img">';
		$html .= 			(has_post_thumbnail() ? '<a class="group" href="#post-' . $id . '">' . get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img(get_field('lien_dachat'), true) : '');
		$html .= '		</div>';
		$html .= '		<a class="group" href="#post-' . $id . '">';
		$html .= 			($type ? '<p class="type-content">' . $type . '</p>' : '');
		$html .=  $title;
		$html .=  $author;
		$html .= '		</a>';
		//$html .= 		get_field('texte_magazine');
		$html .= '	</div>';
		$html .= '</div>';
		display_popup_book();
		echo $html;
	}
	wp_reset_query();
	exit;
}

add_action('wp_ajax_nopriv_get_mag_books', 'get_mag_books');
add_action('wp_ajax_get_mag_books', 'get_mag_books');

function reset_books(){
	header("Content-Type: text/html");
	$args = array(
			'post_type' 	   => 'book',
			'posts_per_page' => 20
	);
	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
	$title = get_the_title();

	//if (strpos($thumb_url[0], 'couvertureNonDisponible')) {
	//} else {

	//}
	$loop = new WP_Query($args);
	while ($loop->have_posts()) { $loop->the_post();
		$eanID = get_field('ean');
		$id = get_the_ID();

		$title =  '<h2 class="entry-title">'.  get_the_title() .'</h2>';
		$author = '<span class="entry-auteur">'.  get_field('auteur') .'</span>';
		$type = get_field('type');
		$html = '<div class="col-md-15 grid-item col-book">';
		$html .= '	<div id="' . $eanID . '" class="entry anchor-ean-archives livres">';
		$html .= '		<div class="img">';
		$html .= 			(has_post_thumbnail() ? '<a class="group" href="#post-' . $id . '">' . get_the_post_thumbnail(null, 'full') . '</a>' . share_btn_img(get_field('lien_dachat'), true) : '');
		$html .= '		</div>';
		$html .= '		<a class="group" href="#post-' . $id . '">';
		$html .= 			($type ? '<p class="type-content">' . $type . '</p>' : '');
		$html .=  $title;
		$html .=  $author;
		$html .= '		</a>';
		//$html .= 		get_field('texte_magazine');
		$html .= '	</div>';
		$html .= '</div>';
		display_popup_book();
		echo $html;
	}
	wp_reset_query();
	exit;
}
add_action('wp_ajax_nopriv_reset_books', 'reset_books');
add_action('wp_ajax_get_reset_books', 'reset_books');


/*
* Creating a function to create our Nos métiers CPT
*/

function nos_metiers_cpt() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Nos metiers', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Nos metiers', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Nos metiers', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent Nos metiers', 'twentythirteen' ),
        'all_items'           => __( 'All Nos metiers', 'twentythirteen' ),
        'view_item'           => __( 'View Nos metiers', 'twentythirteen' ),
        'add_new_item'        => __( 'Add Nos metiers', 'twentythirteen' ),
        'add_new'             => __( 'Add New', 'twentythirteen' ),
        'edit_item'           => __( 'Edit Nos metiers', 'twentythirteen' ),
        'update_item'         => __( 'Update Nos metiers', 'twentythirteen' ),
        'search_items'        => __( 'Search Nos metiers', 'twentythirteen' ),
        'not_found'           => __( 'Not Found', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'nos_metiers', 'twentythirteen' ),
        'description'         => __( 'Nos Metiers', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        //'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 26,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    // Registering your Custom Post Type
    register_post_type( 'nos_metiers', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'nos_metiers_cpt', 0 );
